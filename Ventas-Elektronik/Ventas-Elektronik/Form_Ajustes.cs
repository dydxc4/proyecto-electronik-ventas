﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Ventas_Elektronik
{
    public partial class Form_Ajustes : Form
    {
        AdMon adMon = Programa.AdMon;

        /// <summary>
        /// Constructor principal.
        /// </summary>
        public Form_Ajustes()
        {
            InitializeComponent();
            Selector_IVA.Value = adMon.IVA;
        }

        /// <summary>
        /// Abre la carpeta de ajustes.
        /// </summary>
        private void Boton_AbrirCarpeta_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Path.GetDirectoryName(adMon.RutaDatos)))
            {
                Process.Start(Path.GetDirectoryName(adMon.RutaDatos));
            }
        }

        /// <summary>
        /// Borra los datos guardados.
        /// </summary>
        private void Boton_BorrarAjustes_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Al eliminar los datos la aplicación se cerrará y tendrá que empezar de cero.\n" +
                "¿Está seguro?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                try
                {
                    File.Delete(adMon.RutaDatos);
                    Application.Exit();
                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message);
                }
            }
        }

        /// <summary>
        /// Actualiza el IVA.
        /// </summary>
        private void Boton_Aplicar_Click(object sender, EventArgs e)
        {
            adMon.IVA = Convert.ToInt32(Selector_IVA.Value);
        }
    }
}
