﻿using System;
using System.Windows.Forms;

namespace Ventas_Elektronik
{
    public partial class Form_Ingresos : Form
    {
        private readonly AdMon adMon = Programa.AdMon;
        private float _total, _IVA, _neto, _hoy, _anio, _mes;

        public Form_Ingresos()
        {
            InitializeComponent();
            _total = 0;
            _IVA = 0;
            _neto = 0;
            _hoy = 0;
            _anio = 0;
            _mes = 0;

            foreach (Venta venta in adMon.Ventas)
            {
                float totalVenta = venta.ObtenerTotalVenta();
                _total += totalVenta;
                _neto += venta.ObtenerImporteVenta();

                if (venta.Fecha.Day == DateTime.Now.Day)
                {
                    _hoy += totalVenta;
                }
                if (venta.Fecha.Month == DateTime.Now.Month && venta.Fecha.Year == DateTime.Now.Year)
                {
                    _mes += totalVenta;
                }
                if (venta.Fecha.Year == DateTime.Now.Year)
                {
                    _anio += totalVenta;
                }
            }
            _IVA = _total - _neto;
            ActualizarCampos();
        }

        private void ActualizarCampos()
        {
            TextBox_Total.Text = _total.ToString(adMon.FormatoMoneda);
            TextBox_Neto.Text = _neto.ToString(adMon.FormatoMoneda);
            TextBox_TotalIVA.Text = _IVA.ToString(adMon.FormatoMoneda);
            TextBox_Hoy.Text = _hoy.ToString(adMon.FormatoMoneda);
            TextBox_Mes.Text = _mes.ToString(adMon.FormatoMoneda);
            TextBox_Anio.Text = _anio.ToString(adMon.FormatoMoneda);
        }
    }
}
