﻿namespace Ventas_Elektronik
{
    partial class Form_PuntoVenta
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Etiqueta_Fecha = new System.Windows.Forms.Label();
            this.TextBox_ID = new System.Windows.Forms.TextBox();
            this.Etiqueta_IDVenta = new System.Windows.Forms.Label();
            this.TextBox_Fecha = new System.Windows.Forms.TextBox();
            this.ListView_Articulos = new System.Windows.Forms.ListView();
            this.Columna_ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_Articulo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_Cantidad = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_Precio = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_PrecioConIVA = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_SubtotalConIVA = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TextBox_Entrada = new System.Windows.Forms.TextBox();
            this.Etiqueta_Comando = new System.Windows.Forms.Label();
            this.Boton_Enter = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBox_Total = new System.Windows.Forms.TextBox();
            this.Etiqueta_Info = new System.Windows.Forms.Label();
            this.Boton_ConcretarVenta = new System.Windows.Forms.Button();
            this.Etiqueta_Importe = new System.Windows.Forms.Label();
            this.TextBox_IVA = new System.Windows.Forms.TextBox();
            this.Etiqueta_IVA = new System.Windows.Forms.Label();
            this.TextBox_Importe = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Etiqueta_Fecha
            // 
            this.Etiqueta_Fecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Etiqueta_Fecha.AutoSize = true;
            this.Etiqueta_Fecha.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Fecha.Location = new System.Drawing.Point(849, 70);
            this.Etiqueta_Fecha.Name = "Etiqueta_Fecha";
            this.Etiqueta_Fecha.Size = new System.Drawing.Size(47, 19);
            this.Etiqueta_Fecha.TabIndex = 7;
            this.Etiqueta_Fecha.Text = "Fecha";
            // 
            // TextBox_ID
            // 
            this.TextBox_ID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_ID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_ID.Location = new System.Drawing.Point(852, 34);
            this.TextBox_ID.Name = "TextBox_ID";
            this.TextBox_ID.ReadOnly = true;
            this.TextBox_ID.Size = new System.Drawing.Size(150, 27);
            this.TextBox_ID.TabIndex = 6;
            // 
            // Etiqueta_IDVenta
            // 
            this.Etiqueta_IDVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Etiqueta_IDVenta.AutoSize = true;
            this.Etiqueta_IDVenta.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_IDVenta.Location = new System.Drawing.Point(849, 12);
            this.Etiqueta_IDVenta.Name = "Etiqueta_IDVenta";
            this.Etiqueta_IDVenta.Size = new System.Drawing.Size(85, 19);
            this.Etiqueta_IDVenta.TabIndex = 5;
            this.Etiqueta_IDVenta.Text = "Id. de venta";
            // 
            // TextBox_Fecha
            // 
            this.TextBox_Fecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Fecha.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Fecha.Location = new System.Drawing.Point(852, 92);
            this.TextBox_Fecha.Name = "TextBox_Fecha";
            this.TextBox_Fecha.ReadOnly = true;
            this.TextBox_Fecha.Size = new System.Drawing.Size(150, 27);
            this.TextBox_Fecha.TabIndex = 8;
            // 
            // ListView_Articulos
            // 
            this.ListView_Articulos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListView_Articulos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Columna_ID,
            this.Columna_Articulo,
            this.Columna_Cantidad,
            this.Columna_Precio,
            this.Columna_PrecioConIVA,
            this.Columna_SubtotalConIVA});
            this.ListView_Articulos.Font = new System.Drawing.Font("Calibri", 12F);
            this.ListView_Articulos.FullRowSelect = true;
            this.ListView_Articulos.GridLines = true;
            this.ListView_Articulos.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListView_Articulos.HideSelection = false;
            this.ListView_Articulos.Location = new System.Drawing.Point(12, 12);
            this.ListView_Articulos.MultiSelect = false;
            this.ListView_Articulos.Name = "ListView_Articulos";
            this.ListView_Articulos.Size = new System.Drawing.Size(835, 480);
            this.ListView_Articulos.TabIndex = 0;
            this.ListView_Articulos.UseCompatibleStateImageBehavior = false;
            this.ListView_Articulos.View = System.Windows.Forms.View.Details;
            // 
            // Columna_ID
            // 
            this.Columna_ID.Text = "Id.";
            this.Columna_ID.Width = 50;
            // 
            // Columna_Articulo
            // 
            this.Columna_Articulo.Text = "Artículo";
            this.Columna_Articulo.Width = 300;
            // 
            // Columna_Cantidad
            // 
            this.Columna_Cantidad.Text = "Cantidad";
            this.Columna_Cantidad.Width = 100;
            // 
            // Columna_Precio
            // 
            this.Columna_Precio.Text = "Precio Unidad";
            this.Columna_Precio.Width = 120;
            // 
            // Columna_PrecioConIVA
            // 
            this.Columna_PrecioConIVA.Text = "Precio + IVA";
            this.Columna_PrecioConIVA.Width = 120;
            // 
            // Columna_SubtotalConIVA
            // 
            this.Columna_SubtotalConIVA.Text = "Subtotal + IVA";
            this.Columna_SubtotalConIVA.Width = 120;
            // 
            // TextBox_Entrada
            // 
            this.TextBox_Entrada.AcceptsReturn = true;
            this.TextBox_Entrada.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Entrada.Font = new System.Drawing.Font("Calibri", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Entrada.Location = new System.Drawing.Point(121, 499);
            this.TextBox_Entrada.Multiline = true;
            this.TextBox_Entrada.Name = "TextBox_Entrada";
            this.TextBox_Entrada.Size = new System.Drawing.Size(570, 33);
            this.TextBox_Entrada.TabIndex = 2;
            this.TextBox_Entrada.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_Entrada_KeyDown);
            this.TextBox_Entrada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_Entrada_KeyPress);
            // 
            // Etiqueta_Comando
            // 
            this.Etiqueta_Comando.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Etiqueta_Comando.AutoSize = true;
            this.Etiqueta_Comando.Font = new System.Drawing.Font("Calibri Light", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Comando.Location = new System.Drawing.Point(7, 502);
            this.Etiqueta_Comando.Name = "Etiqueta_Comando";
            this.Etiqueta_Comando.Size = new System.Drawing.Size(101, 28);
            this.Etiqueta_Comando.TabIndex = 1;
            this.Etiqueta_Comando.Text = "Comando";
            // 
            // Boton_Enter
            // 
            this.Boton_Enter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Boton_Enter.Location = new System.Drawing.Point(697, 498);
            this.Boton_Enter.Name = "Boton_Enter";
            this.Boton_Enter.Size = new System.Drawing.Size(150, 33);
            this.Boton_Enter.TabIndex = 3;
            this.Boton_Enter.Text = "Enter";
            this.Boton_Enter.UseVisualStyleBackColor = true;
            this.Boton_Enter.Click += new System.EventHandler(this.Boton_Enter_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri Light", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(847, 426);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 28);
            this.label3.TabIndex = 13;
            this.label3.Text = "Total";
            // 
            // TextBox_Total
            // 
            this.TextBox_Total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Total.Font = new System.Drawing.Font("Calibri", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Total.Location = new System.Drawing.Point(852, 457);
            this.TextBox_Total.Name = "TextBox_Total";
            this.TextBox_Total.ReadOnly = true;
            this.TextBox_Total.Size = new System.Drawing.Size(150, 35);
            this.TextBox_Total.TabIndex = 14;
            // 
            // Etiqueta_Info
            // 
            this.Etiqueta_Info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Etiqueta_Info.AutoSize = true;
            this.Etiqueta_Info.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Info.Location = new System.Drawing.Point(8, 530);
            this.Etiqueta_Info.Name = "Etiqueta_Info";
            this.Etiqueta_Info.Size = new System.Drawing.Size(0, 19);
            this.Etiqueta_Info.TabIndex = 15;
            // 
            // Boton_ConcretarVenta
            // 
            this.Boton_ConcretarVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Boton_ConcretarVenta.Location = new System.Drawing.Point(852, 498);
            this.Boton_ConcretarVenta.Name = "Boton_ConcretarVenta";
            this.Boton_ConcretarVenta.Size = new System.Drawing.Size(150, 33);
            this.Boton_ConcretarVenta.TabIndex = 4;
            this.Boton_ConcretarVenta.Text = "Concretar venta";
            this.Boton_ConcretarVenta.UseVisualStyleBackColor = true;
            this.Boton_ConcretarVenta.Click += new System.EventHandler(this.Boton_ConcretarVenta_Click);
            // 
            // Etiqueta_Importe
            // 
            this.Etiqueta_Importe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Etiqueta_Importe.AutoSize = true;
            this.Etiqueta_Importe.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Importe.Location = new System.Drawing.Point(848, 310);
            this.Etiqueta_Importe.Name = "Etiqueta_Importe";
            this.Etiqueta_Importe.Size = new System.Drawing.Size(61, 19);
            this.Etiqueta_Importe.TabIndex = 9;
            this.Etiqueta_Importe.Text = "Importe";
            // 
            // TextBox_IVA
            // 
            this.TextBox_IVA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_IVA.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_IVA.Location = new System.Drawing.Point(852, 390);
            this.TextBox_IVA.Name = "TextBox_IVA";
            this.TextBox_IVA.ReadOnly = true;
            this.TextBox_IVA.Size = new System.Drawing.Size(150, 27);
            this.TextBox_IVA.TabIndex = 12;
            // 
            // Etiqueta_IVA
            // 
            this.Etiqueta_IVA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Etiqueta_IVA.AutoSize = true;
            this.Etiqueta_IVA.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_IVA.Location = new System.Drawing.Point(849, 368);
            this.Etiqueta_IVA.Name = "Etiqueta_IVA";
            this.Etiqueta_IVA.Size = new System.Drawing.Size(30, 19);
            this.Etiqueta_IVA.TabIndex = 11;
            this.Etiqueta_IVA.Text = "IVA";
            // 
            // TextBox_Importe
            // 
            this.TextBox_Importe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Importe.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Importe.Location = new System.Drawing.Point(852, 332);
            this.TextBox_Importe.Name = "TextBox_Importe";
            this.TextBox_Importe.ReadOnly = true;
            this.TextBox_Importe.Size = new System.Drawing.Size(150, 27);
            this.TextBox_Importe.TabIndex = 10;
            // 
            // Form_PuntoVenta
            // 
            this.AcceptButton = this.Boton_Enter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 561);
            this.Controls.Add(this.TextBox_Importe);
            this.Controls.Add(this.TextBox_IVA);
            this.Controls.Add(this.Etiqueta_IVA);
            this.Controls.Add(this.Etiqueta_Importe);
            this.Controls.Add(this.Boton_ConcretarVenta);
            this.Controls.Add(this.Etiqueta_Info);
            this.Controls.Add(this.TextBox_Total);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Boton_Enter);
            this.Controls.Add(this.Etiqueta_Comando);
            this.Controls.Add(this.TextBox_Entrada);
            this.Controls.Add(this.ListView_Articulos);
            this.Controls.Add(this.TextBox_Fecha);
            this.Controls.Add(this.Etiqueta_IDVenta);
            this.Controls.Add(this.TextBox_ID);
            this.Controls.Add(this.Etiqueta_Fecha);
            this.MinimumSize = new System.Drawing.Size(1030, 600);
            this.Name = "Form_PuntoVenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Venta";
            this.Activated += new System.EventHandler(this.Form_Ventas_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Ventas_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Etiqueta_Fecha;
        private System.Windows.Forms.TextBox TextBox_ID;
        private System.Windows.Forms.Label Etiqueta_IDVenta;
        private System.Windows.Forms.TextBox TextBox_Fecha;
        private System.Windows.Forms.ListView ListView_Articulos;
        private System.Windows.Forms.ColumnHeader Columna_ID;
        private System.Windows.Forms.ColumnHeader Columna_Articulo;
        private System.Windows.Forms.ColumnHeader Columna_Cantidad;
        private System.Windows.Forms.ColumnHeader Columna_Precio;
        private System.Windows.Forms.TextBox TextBox_Entrada;
        private System.Windows.Forms.Label Etiqueta_Comando;
        private System.Windows.Forms.Button Boton_Enter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBox_Total;
        private System.Windows.Forms.Label Etiqueta_Info;
        private System.Windows.Forms.Button Boton_ConcretarVenta;
        private System.Windows.Forms.ColumnHeader Columna_SubtotalConIVA;
        private System.Windows.Forms.Label Etiqueta_Importe;
        private System.Windows.Forms.TextBox TextBox_IVA;
        private System.Windows.Forms.Label Etiqueta_IVA;
        private System.Windows.Forms.TextBox TextBox_Importe;
        private System.Windows.Forms.ColumnHeader Columna_PrecioConIVA;
    }
}

