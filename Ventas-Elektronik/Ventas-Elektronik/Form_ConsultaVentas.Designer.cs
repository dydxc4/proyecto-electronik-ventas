﻿namespace Ventas_Elektronik
{
    partial class Form_ConsultaVentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListView_Ventas = new System.Windows.Forms.ListView();
            this.Columna_ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_Fecha = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_Importe = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_IVA = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_Total = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Boton_VerVenta = new System.Windows.Forms.Button();
            this.Boton_Cerrar = new System.Windows.Forms.Button();
            this.Etiqueta_CantidadArticulos = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ListView_Ventas
            // 
            this.ListView_Ventas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListView_Ventas.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Columna_ID,
            this.Columna_Fecha,
            this.Columna_Importe,
            this.Columna_IVA,
            this.Columna_Total});
            this.ListView_Ventas.Font = new System.Drawing.Font("Calibri", 12F);
            this.ListView_Ventas.FullRowSelect = true;
            this.ListView_Ventas.GridLines = true;
            this.ListView_Ventas.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListView_Ventas.HideSelection = false;
            this.ListView_Ventas.Location = new System.Drawing.Point(12, 41);
            this.ListView_Ventas.MultiSelect = false;
            this.ListView_Ventas.Name = "ListView_Ventas";
            this.ListView_Ventas.Size = new System.Drawing.Size(565, 300);
            this.ListView_Ventas.TabIndex = 0;
            this.ListView_Ventas.UseCompatibleStateImageBehavior = false;
            this.ListView_Ventas.View = System.Windows.Forms.View.Details;
            this.ListView_Ventas.DoubleClick += new System.EventHandler(this.ListView_Ventas_DoubleClick);
            // 
            // Columna_ID
            // 
            this.Columna_ID.Text = "Id.";
            // 
            // Columna_Fecha
            // 
            this.Columna_Fecha.Text = "Fecha";
            this.Columna_Fecha.Width = 120;
            // 
            // Columna_Importe
            // 
            this.Columna_Importe.Text = "Importe";
            this.Columna_Importe.Width = 120;
            // 
            // Columna_IVA
            // 
            this.Columna_IVA.Text = "IVA";
            this.Columna_IVA.Width = 120;
            // 
            // Columna_Total
            // 
            this.Columna_Total.Text = "Total";
            this.Columna_Total.Width = 120;
            // 
            // Boton_VerVenta
            // 
            this.Boton_VerVenta.Location = new System.Drawing.Point(12, 12);
            this.Boton_VerVenta.Name = "Boton_VerVenta";
            this.Boton_VerVenta.Size = new System.Drawing.Size(150, 23);
            this.Boton_VerVenta.TabIndex = 13;
            this.Boton_VerVenta.Text = "Ver venta seleccionada";
            this.Boton_VerVenta.UseVisualStyleBackColor = true;
            this.Boton_VerVenta.Click += new System.EventHandler(this.Boton_VerVenta_Click);
            // 
            // Boton_Cerrar
            // 
            this.Boton_Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Boton_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Boton_Cerrar.Location = new System.Drawing.Point(502, 347);
            this.Boton_Cerrar.Name = "Boton_Cerrar";
            this.Boton_Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Boton_Cerrar.TabIndex = 14;
            this.Boton_Cerrar.Text = "Cerrar";
            this.Boton_Cerrar.UseVisualStyleBackColor = true;
            // 
            // Etiqueta_CantidadArticulos
            // 
            this.Etiqueta_CantidadArticulos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Etiqueta_CantidadArticulos.AutoSize = true;
            this.Etiqueta_CantidadArticulos.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_CantidadArticulos.Location = new System.Drawing.Point(8, 348);
            this.Etiqueta_CantidadArticulos.Name = "Etiqueta_CantidadArticulos";
            this.Etiqueta_CantidadArticulos.Size = new System.Drawing.Size(0, 19);
            this.Etiqueta_CantidadArticulos.TabIndex = 15;
            // 
            // Form_ConsultaVentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Boton_Cerrar;
            this.ClientSize = new System.Drawing.Size(594, 381);
            this.Controls.Add(this.Etiqueta_CantidadArticulos);
            this.Controls.Add(this.Boton_Cerrar);
            this.Controls.Add(this.Boton_VerVenta);
            this.Controls.Add(this.ListView_Ventas);
            this.MaximizeBox = false;
            this.Name = "Form_ConsultaVentas";
            this.Text = "Consulta de ventas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView ListView_Ventas;
        private System.Windows.Forms.ColumnHeader Columna_ID;
        private System.Windows.Forms.ColumnHeader Columna_Fecha;
        private System.Windows.Forms.ColumnHeader Columna_Importe;
        private System.Windows.Forms.ColumnHeader Columna_IVA;
        private System.Windows.Forms.Button Boton_VerVenta;
        private System.Windows.Forms.Button Boton_Cerrar;
        private System.Windows.Forms.ColumnHeader Columna_Total;
        private System.Windows.Forms.Label Etiqueta_CantidadArticulos;
    }
}