﻿namespace Ventas_Elektronik
{
    partial class Form_Ajustes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Selector_IVA = new System.Windows.Forms.NumericUpDown();
            this.Boton_AbrirCarpeta = new System.Windows.Forms.Button();
            this.Boton_BorrarAjustes = new System.Windows.Forms.Button();
            this.Boton_Cerrar = new System.Windows.Forms.Button();
            this.Boton_Aplicar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Selector_IVA)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "IVA";
            // 
            // Selector_IVA
            // 
            this.Selector_IVA.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Selector_IVA.Location = new System.Drawing.Point(55, 12);
            this.Selector_IVA.Name = "Selector_IVA";
            this.Selector_IVA.Size = new System.Drawing.Size(51, 27);
            this.Selector_IVA.TabIndex = 1;
            // 
            // Boton_AbrirCarpeta
            // 
            this.Boton_AbrirCarpeta.Location = new System.Drawing.Point(12, 45);
            this.Boton_AbrirCarpeta.Name = "Boton_AbrirCarpeta";
            this.Boton_AbrirCarpeta.Size = new System.Drawing.Size(150, 23);
            this.Boton_AbrirCarpeta.TabIndex = 2;
            this.Boton_AbrirCarpeta.Text = "Abrir carpeta de ajustes";
            this.Boton_AbrirCarpeta.UseVisualStyleBackColor = true;
            this.Boton_AbrirCarpeta.Click += new System.EventHandler(this.Boton_AbrirCarpeta_Click);
            // 
            // Boton_BorrarAjustes
            // 
            this.Boton_BorrarAjustes.Location = new System.Drawing.Point(12, 74);
            this.Boton_BorrarAjustes.Name = "Boton_BorrarAjustes";
            this.Boton_BorrarAjustes.Size = new System.Drawing.Size(150, 23);
            this.Boton_BorrarAjustes.TabIndex = 3;
            this.Boton_BorrarAjustes.Text = "Borrar datos de la aplicación";
            this.Boton_BorrarAjustes.UseVisualStyleBackColor = true;
            this.Boton_BorrarAjustes.Click += new System.EventHandler(this.Boton_BorrarAjustes_Click);
            // 
            // Boton_Cerrar
            // 
            this.Boton_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Boton_Cerrar.Location = new System.Drawing.Point(12, 103);
            this.Boton_Cerrar.Name = "Boton_Cerrar";
            this.Boton_Cerrar.Size = new System.Drawing.Size(150, 23);
            this.Boton_Cerrar.TabIndex = 4;
            this.Boton_Cerrar.Text = "Cerrar";
            this.Boton_Cerrar.UseVisualStyleBackColor = true;
            // 
            // Boton_Aplicar
            // 
            this.Boton_Aplicar.Location = new System.Drawing.Point(112, 13);
            this.Boton_Aplicar.Name = "Boton_Aplicar";
            this.Boton_Aplicar.Size = new System.Drawing.Size(50, 23);
            this.Boton_Aplicar.TabIndex = 5;
            this.Boton_Aplicar.Text = "Aplicar";
            this.Boton_Aplicar.UseVisualStyleBackColor = true;
            this.Boton_Aplicar.Click += new System.EventHandler(this.Boton_Aplicar_Click);
            // 
            // Form_Ajustes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Boton_Cerrar;
            this.ClientSize = new System.Drawing.Size(172, 133);
            this.Controls.Add(this.Boton_Aplicar);
            this.Controls.Add(this.Boton_Cerrar);
            this.Controls.Add(this.Boton_BorrarAjustes);
            this.Controls.Add(this.Boton_AbrirCarpeta);
            this.Controls.Add(this.Selector_IVA);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Ajustes";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajustes";
            ((System.ComponentModel.ISupportInitialize)(this.Selector_IVA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown Selector_IVA;
        private System.Windows.Forms.Button Boton_AbrirCarpeta;
        private System.Windows.Forms.Button Boton_BorrarAjustes;
        private System.Windows.Forms.Button Boton_Cerrar;
        private System.Windows.Forms.Button Boton_Aplicar;
    }
}