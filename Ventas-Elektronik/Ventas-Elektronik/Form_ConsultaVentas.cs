﻿using System;
using System.Windows.Forms;

namespace Ventas_Elektronik
{
    public partial class Form_ConsultaVentas : Form
    {
        private readonly AdMon adMon = Programa.AdMon;

        public Form_ConsultaVentas()
        {
            InitializeComponent();
            CargarElementosListView();
            Etiqueta_CantidadArticulos.Text = $"{adMon.Ventas.Count} ventas registradas.";
        }

        /// <summary>
        /// Carga los artículos dentro de un ListView.
        /// </summary>
        private void CargarElementosListView()
        {
            foreach (Venta venta in adMon.Ventas)
            {
                float iva, importe, total;
                importe = venta.ObtenerImporteVenta();
                total = venta.ObtenerTotalVenta();
                iva = total - importe;

                string[] lineas = new string[5];
                lineas[0] = venta.ID.ToString(); // ID
                lineas[1] = venta.Fecha.ToString(adMon.FormatoFecha); // Fecha
                lineas[2] = importe.ToString(adMon.FormatoMoneda); // Importe
                lineas[4] = total.ToString(adMon.FormatoMoneda); // Total
                lineas[3] = iva.ToString(adMon.FormatoMoneda);

                ListView_Ventas.Items.Add(new ListViewItem(lineas));
            }
            ListView_Ventas.Update();
        }

        /// <summary>
        /// Abre una nueva ventana con la venta seleccionada.
        /// </summary>
        private void MostrarVenta()
        {
            if (ListView_Ventas.SelectedItems.Count == 1)
            {
                Form_PuntoVenta form = new Form_PuntoVenta(Convert.ToInt32(ListView_Ventas.SelectedItems[0].Text))
                {
                    StartPosition = FormStartPosition.WindowsDefaultBounds
                };
                form.Show();

            }
        }

        #region Eventos

        private void Boton_VerVenta_Click(object sender, EventArgs e)
        {
            MostrarVenta();
        }

        private void ListView_Ventas_DoubleClick(object sender, EventArgs e)
        {
            MostrarVenta();
        }

        #endregion
    }
}
