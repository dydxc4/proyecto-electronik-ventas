﻿namespace Ventas_Elektronik
{
    partial class Form_Articulos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListView_Articulos = new System.Windows.Forms.ListView();
            this.Columna_ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_Nombre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_PrecioSinIVA = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Columna_PrecioFinal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Boton_Nuevo = new System.Windows.Forms.Button();
            this.Boton_Editar = new System.Windows.Forms.Button();
            this.Boton_Eliminar = new System.Windows.Forms.Button();
            this.Boton_Cerrar = new System.Windows.Forms.Button();
            this.Etiqueta_CantidadArticulos = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ListView_Articulos
            // 
            this.ListView_Articulos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListView_Articulos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Columna_ID,
            this.Columna_Nombre,
            this.Columna_PrecioSinIVA,
            this.Columna_PrecioFinal});
            this.ListView_Articulos.Font = new System.Drawing.Font("Calibri", 12F);
            this.ListView_Articulos.FullRowSelect = true;
            this.ListView_Articulos.GridLines = true;
            this.ListView_Articulos.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListView_Articulos.HideSelection = false;
            this.ListView_Articulos.Location = new System.Drawing.Point(12, 41);
            this.ListView_Articulos.MultiSelect = false;
            this.ListView_Articulos.Name = "ListView_Articulos";
            this.ListView_Articulos.Size = new System.Drawing.Size(620, 341);
            this.ListView_Articulos.TabIndex = 0;
            this.ListView_Articulos.UseCompatibleStateImageBehavior = false;
            this.ListView_Articulos.View = System.Windows.Forms.View.Details;
            this.ListView_Articulos.DoubleClick += new System.EventHandler(this.ListView_Articulos_DoubleClick);
            // 
            // Columna_ID
            // 
            this.Columna_ID.Text = "Id.";
            this.Columna_ID.Width = 50;
            // 
            // Columna_Nombre
            // 
            this.Columna_Nombre.Text = "Nombre";
            this.Columna_Nombre.Width = 300;
            // 
            // Columna_PrecioSinIVA
            // 
            this.Columna_PrecioSinIVA.Text = "Precio sin IVA";
            this.Columna_PrecioSinIVA.Width = 120;
            // 
            // Columna_PrecioFinal
            // 
            this.Columna_PrecioFinal.Text = "Precio final";
            this.Columna_PrecioFinal.Width = 120;
            // 
            // Boton_Nuevo
            // 
            this.Boton_Nuevo.Location = new System.Drawing.Point(12, 12);
            this.Boton_Nuevo.Name = "Boton_Nuevo";
            this.Boton_Nuevo.Size = new System.Drawing.Size(75, 23);
            this.Boton_Nuevo.TabIndex = 1;
            this.Boton_Nuevo.Text = "Nuevo";
            this.Boton_Nuevo.UseVisualStyleBackColor = true;
            this.Boton_Nuevo.Click += new System.EventHandler(this.Boton_Nuevo_Click);
            // 
            // Boton_Editar
            // 
            this.Boton_Editar.Location = new System.Drawing.Point(93, 12);
            this.Boton_Editar.Name = "Boton_Editar";
            this.Boton_Editar.Size = new System.Drawing.Size(75, 23);
            this.Boton_Editar.TabIndex = 2;
            this.Boton_Editar.Text = "Editar";
            this.Boton_Editar.UseVisualStyleBackColor = true;
            this.Boton_Editar.Click += new System.EventHandler(this.Boton_Editar_Click);
            // 
            // Boton_Eliminar
            // 
            this.Boton_Eliminar.Location = new System.Drawing.Point(174, 12);
            this.Boton_Eliminar.Name = "Boton_Eliminar";
            this.Boton_Eliminar.Size = new System.Drawing.Size(75, 23);
            this.Boton_Eliminar.TabIndex = 3;
            this.Boton_Eliminar.Text = "Eliminar";
            this.Boton_Eliminar.UseVisualStyleBackColor = true;
            this.Boton_Eliminar.Click += new System.EventHandler(this.Boton_Eliminar_Click);
            // 
            // Boton_Cerrar
            // 
            this.Boton_Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Boton_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Boton_Cerrar.Location = new System.Drawing.Point(557, 388);
            this.Boton_Cerrar.Name = "Boton_Cerrar";
            this.Boton_Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Boton_Cerrar.TabIndex = 5;
            this.Boton_Cerrar.Text = "Cerrar";
            this.Boton_Cerrar.UseVisualStyleBackColor = true;
            // 
            // Etiqueta_CantidadArticulos
            // 
            this.Etiqueta_CantidadArticulos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Etiqueta_CantidadArticulos.AutoSize = true;
            this.Etiqueta_CantidadArticulos.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_CantidadArticulos.Location = new System.Drawing.Point(8, 389);
            this.Etiqueta_CantidadArticulos.Name = "Etiqueta_CantidadArticulos";
            this.Etiqueta_CantidadArticulos.Size = new System.Drawing.Size(0, 19);
            this.Etiqueta_CantidadArticulos.TabIndex = 4;
            // 
            // Form_Articulos
            // 
            this.AcceptButton = this.Boton_Nuevo;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Boton_Cerrar;
            this.ClientSize = new System.Drawing.Size(644, 421);
            this.Controls.Add(this.Etiqueta_CantidadArticulos);
            this.Controls.Add(this.Boton_Cerrar);
            this.Controls.Add(this.Boton_Eliminar);
            this.Controls.Add(this.Boton_Editar);
            this.Controls.Add(this.Boton_Nuevo);
            this.Controls.Add(this.ListView_Articulos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form_Articulos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Artículos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView ListView_Articulos;
        private System.Windows.Forms.ColumnHeader Columna_ID;
        private System.Windows.Forms.ColumnHeader Columna_Nombre;
        private System.Windows.Forms.ColumnHeader Columna_PrecioSinIVA;
        private System.Windows.Forms.Button Boton_Nuevo;
        private System.Windows.Forms.Button Boton_Editar;
        private System.Windows.Forms.Button Boton_Eliminar;
        private System.Windows.Forms.Button Boton_Cerrar;
        private System.Windows.Forms.Label Etiqueta_CantidadArticulos;
        private System.Windows.Forms.ColumnHeader Columna_PrecioFinal;
    }
}