﻿namespace Ventas_Elektronik
{
    partial class Form_EdicionArticulo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Etiqueta_IdArticulo = new System.Windows.Forms.Label();
            this.Etiqueta_Nombre = new System.Windows.Forms.Label();
            this.Etiqueta_PrecioConIVA = new System.Windows.Forms.Label();
            this.TextBox_ID = new System.Windows.Forms.TextBox();
            this.TextBox_Nombre = new System.Windows.Forms.TextBox();
            this.TextBox_PrecioConIVA = new System.Windows.Forms.TextBox();
            this.Boton_Aceptar = new System.Windows.Forms.Button();
            this.Boton_Cancelar = new System.Windows.Forms.Button();
            this.TextBox_IVA = new System.Windows.Forms.TextBox();
            this.Etiqueta_IVA = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Etiqueta_IdArticulo
            // 
            this.Etiqueta_IdArticulo.AutoSize = true;
            this.Etiqueta_IdArticulo.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_IdArticulo.Location = new System.Drawing.Point(12, 15);
            this.Etiqueta_IdArticulo.Name = "Etiqueta_IdArticulo";
            this.Etiqueta_IdArticulo.Size = new System.Drawing.Size(99, 19);
            this.Etiqueta_IdArticulo.TabIndex = 0;
            this.Etiqueta_IdArticulo.Text = "Id. de artículo";
            // 
            // Etiqueta_Nombre
            // 
            this.Etiqueta_Nombre.AutoSize = true;
            this.Etiqueta_Nombre.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Nombre.Location = new System.Drawing.Point(12, 48);
            this.Etiqueta_Nombre.Name = "Etiqueta_Nombre";
            this.Etiqueta_Nombre.Size = new System.Drawing.Size(62, 19);
            this.Etiqueta_Nombre.TabIndex = 2;
            this.Etiqueta_Nombre.Text = "Nombre";
            // 
            // Etiqueta_PrecioConIVA
            // 
            this.Etiqueta_PrecioConIVA.AutoSize = true;
            this.Etiqueta_PrecioConIVA.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_PrecioConIVA.Location = new System.Drawing.Point(12, 81);
            this.Etiqueta_PrecioConIVA.Name = "Etiqueta_PrecioConIVA";
            this.Etiqueta_PrecioConIVA.Size = new System.Drawing.Size(102, 19);
            this.Etiqueta_PrecioConIVA.TabIndex = 4;
            this.Etiqueta_PrecioConIVA.Text = "Precio con IVA";
            // 
            // TextBox_ID
            // 
            this.TextBox_ID.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_ID.Location = new System.Drawing.Point(127, 12);
            this.TextBox_ID.Name = "TextBox_ID";
            this.TextBox_ID.ReadOnly = true;
            this.TextBox_ID.Size = new System.Drawing.Size(200, 27);
            this.TextBox_ID.TabIndex = 1;
            // 
            // TextBox_Nombre
            // 
            this.TextBox_Nombre.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Nombre.Location = new System.Drawing.Point(127, 45);
            this.TextBox_Nombre.Name = "TextBox_Nombre";
            this.TextBox_Nombre.Size = new System.Drawing.Size(200, 27);
            this.TextBox_Nombre.TabIndex = 3;
            this.TextBox_Nombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_Nombre_KeyPress);
            // 
            // TextBox_PrecioConIVA
            // 
            this.TextBox_PrecioConIVA.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_PrecioConIVA.Location = new System.Drawing.Point(127, 78);
            this.TextBox_PrecioConIVA.Name = "TextBox_PrecioConIVA";
            this.TextBox_PrecioConIVA.Size = new System.Drawing.Size(200, 27);
            this.TextBox_PrecioConIVA.TabIndex = 5;
            this.TextBox_PrecioConIVA.TextChanged += new System.EventHandler(this.TextBox_PrecioConIVA_TextChanged);
            this.TextBox_PrecioConIVA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_PrecioConIVA_KeyPress);
            // 
            // Boton_Aceptar
            // 
            this.Boton_Aceptar.Location = new System.Drawing.Point(252, 151);
            this.Boton_Aceptar.Name = "Boton_Aceptar";
            this.Boton_Aceptar.Size = new System.Drawing.Size(75, 23);
            this.Boton_Aceptar.TabIndex = 9;
            this.Boton_Aceptar.Text = "Aceptar";
            this.Boton_Aceptar.UseVisualStyleBackColor = true;
            this.Boton_Aceptar.Click += new System.EventHandler(this.Boton_Aceptar_Click);
            // 
            // Boton_Cancelar
            // 
            this.Boton_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Boton_Cancelar.Location = new System.Drawing.Point(171, 151);
            this.Boton_Cancelar.Name = "Boton_Cancelar";
            this.Boton_Cancelar.Size = new System.Drawing.Size(75, 23);
            this.Boton_Cancelar.TabIndex = 8;
            this.Boton_Cancelar.Text = "Cancelar";
            this.Boton_Cancelar.UseVisualStyleBackColor = true;
            // 
            // TextBox_IVA
            // 
            this.TextBox_IVA.Enabled = false;
            this.TextBox_IVA.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_IVA.Location = new System.Drawing.Point(127, 111);
            this.TextBox_IVA.Name = "TextBox_IVA";
            this.TextBox_IVA.ReadOnly = true;
            this.TextBox_IVA.Size = new System.Drawing.Size(200, 27);
            this.TextBox_IVA.TabIndex = 7;
            // 
            // Etiqueta_IVA
            // 
            this.Etiqueta_IVA.AutoSize = true;
            this.Etiqueta_IVA.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_IVA.Location = new System.Drawing.Point(12, 114);
            this.Etiqueta_IVA.Name = "Etiqueta_IVA";
            this.Etiqueta_IVA.Size = new System.Drawing.Size(30, 19);
            this.Etiqueta_IVA.TabIndex = 6;
            this.Etiqueta_IVA.Text = "IVA";
            // 
            // Form_EdicionArticulo
            // 
            this.AcceptButton = this.Boton_Aceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Boton_Cancelar;
            this.ClientSize = new System.Drawing.Size(339, 186);
            this.Controls.Add(this.Etiqueta_IVA);
            this.Controls.Add(this.TextBox_IVA);
            this.Controls.Add(this.Boton_Cancelar);
            this.Controls.Add(this.Boton_Aceptar);
            this.Controls.Add(this.TextBox_PrecioConIVA);
            this.Controls.Add(this.TextBox_Nombre);
            this.Controls.Add(this.TextBox_ID);
            this.Controls.Add(this.Etiqueta_PrecioConIVA);
            this.Controls.Add(this.Etiqueta_Nombre);
            this.Controls.Add(this.Etiqueta_IdArticulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_EdicionArticulo";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edición de artículo";
            this.Activated += new System.EventHandler(this.Form_EdicionArticulo_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Etiqueta_IdArticulo;
        private System.Windows.Forms.Label Etiqueta_Nombre;
        private System.Windows.Forms.Label Etiqueta_PrecioConIVA;
        private System.Windows.Forms.TextBox TextBox_ID;
        private System.Windows.Forms.TextBox TextBox_Nombre;
        private System.Windows.Forms.TextBox TextBox_PrecioConIVA;
        private System.Windows.Forms.Button Boton_Aceptar;
        private System.Windows.Forms.Button Boton_Cancelar;
        private System.Windows.Forms.TextBox TextBox_IVA;
        private System.Windows.Forms.Label Etiqueta_IVA;
    }
}