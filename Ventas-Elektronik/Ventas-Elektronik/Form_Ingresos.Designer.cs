﻿namespace Ventas_Elektronik
{
    partial class Form_Ingresos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Grupo_IngresoFecha = new System.Windows.Forms.GroupBox();
            this.Etiqueta_Hoy = new System.Windows.Forms.Label();
            this.TextBox_Mes = new System.Windows.Forms.TextBox();
            this.Etiqueta_Mes = new System.Windows.Forms.Label();
            this.Etiqueta_Anio = new System.Windows.Forms.Label();
            this.TextBox_Anio = new System.Windows.Forms.TextBox();
            this.TextBox_Hoy = new System.Windows.Forms.TextBox();
            this.Grupo_IngresoGeneral = new System.Windows.Forms.GroupBox();
            this.Etiqueta_Total = new System.Windows.Forms.Label();
            this.TextBox_TotalIVA = new System.Windows.Forms.TextBox();
            this.Etiqueta_Neto = new System.Windows.Forms.Label();
            this.Etiqueta_TotalIVA = new System.Windows.Forms.Label();
            this.TextBox_Neto = new System.Windows.Forms.TextBox();
            this.TextBox_Total = new System.Windows.Forms.TextBox();
            this.Boton_Cerrar = new System.Windows.Forms.Button();
            this.Grupo_IngresoFecha.SuspendLayout();
            this.Grupo_IngresoGeneral.SuspendLayout();
            this.SuspendLayout();
            // 
            // Grupo_IngresoFecha
            // 
            this.Grupo_IngresoFecha.Controls.Add(this.Etiqueta_Hoy);
            this.Grupo_IngresoFecha.Controls.Add(this.TextBox_Mes);
            this.Grupo_IngresoFecha.Controls.Add(this.Etiqueta_Mes);
            this.Grupo_IngresoFecha.Controls.Add(this.Etiqueta_Anio);
            this.Grupo_IngresoFecha.Controls.Add(this.TextBox_Anio);
            this.Grupo_IngresoFecha.Controls.Add(this.TextBox_Hoy);
            this.Grupo_IngresoFecha.Location = new System.Drawing.Point(12, 143);
            this.Grupo_IngresoFecha.Name = "Grupo_IngresoFecha";
            this.Grupo_IngresoFecha.Size = new System.Drawing.Size(250, 125);
            this.Grupo_IngresoFecha.TabIndex = 1;
            this.Grupo_IngresoFecha.TabStop = false;
            this.Grupo_IngresoFecha.Text = "Ingreso por fecha";
            // 
            // Etiqueta_Hoy
            // 
            this.Etiqueta_Hoy.AutoSize = true;
            this.Etiqueta_Hoy.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Hoy.Location = new System.Drawing.Point(6, 22);
            this.Etiqueta_Hoy.Name = "Etiqueta_Hoy";
            this.Etiqueta_Hoy.Size = new System.Drawing.Size(34, 19);
            this.Etiqueta_Hoy.TabIndex = 0;
            this.Etiqueta_Hoy.Text = "Hoy";
            // 
            // TextBox_Mes
            // 
            this.TextBox_Mes.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Mes.Location = new System.Drawing.Point(144, 52);
            this.TextBox_Mes.Name = "TextBox_Mes";
            this.TextBox_Mes.ReadOnly = true;
            this.TextBox_Mes.Size = new System.Drawing.Size(100, 27);
            this.TextBox_Mes.TabIndex = 3;
            // 
            // Etiqueta_Mes
            // 
            this.Etiqueta_Mes.AutoSize = true;
            this.Etiqueta_Mes.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Mes.Location = new System.Drawing.Point(6, 55);
            this.Etiqueta_Mes.Name = "Etiqueta_Mes";
            this.Etiqueta_Mes.Size = new System.Drawing.Size(37, 19);
            this.Etiqueta_Mes.TabIndex = 2;
            this.Etiqueta_Mes.Text = "Mes";
            // 
            // Etiqueta_Anio
            // 
            this.Etiqueta_Anio.AutoSize = true;
            this.Etiqueta_Anio.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Anio.Location = new System.Drawing.Point(6, 88);
            this.Etiqueta_Anio.Name = "Etiqueta_Anio";
            this.Etiqueta_Anio.Size = new System.Drawing.Size(34, 19);
            this.Etiqueta_Anio.TabIndex = 4;
            this.Etiqueta_Anio.Text = "Año";
            // 
            // TextBox_Anio
            // 
            this.TextBox_Anio.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Anio.Location = new System.Drawing.Point(144, 85);
            this.TextBox_Anio.Name = "TextBox_Anio";
            this.TextBox_Anio.ReadOnly = true;
            this.TextBox_Anio.Size = new System.Drawing.Size(100, 27);
            this.TextBox_Anio.TabIndex = 5;
            // 
            // TextBox_Hoy
            // 
            this.TextBox_Hoy.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Hoy.Location = new System.Drawing.Point(144, 19);
            this.TextBox_Hoy.Name = "TextBox_Hoy";
            this.TextBox_Hoy.ReadOnly = true;
            this.TextBox_Hoy.Size = new System.Drawing.Size(100, 27);
            this.TextBox_Hoy.TabIndex = 1;
            // 
            // Grupo_IngresoGeneral
            // 
            this.Grupo_IngresoGeneral.Controls.Add(this.Etiqueta_Total);
            this.Grupo_IngresoGeneral.Controls.Add(this.TextBox_TotalIVA);
            this.Grupo_IngresoGeneral.Controls.Add(this.Etiqueta_Neto);
            this.Grupo_IngresoGeneral.Controls.Add(this.Etiqueta_TotalIVA);
            this.Grupo_IngresoGeneral.Controls.Add(this.TextBox_Neto);
            this.Grupo_IngresoGeneral.Controls.Add(this.TextBox_Total);
            this.Grupo_IngresoGeneral.Location = new System.Drawing.Point(12, 12);
            this.Grupo_IngresoGeneral.Name = "Grupo_IngresoGeneral";
            this.Grupo_IngresoGeneral.Size = new System.Drawing.Size(250, 125);
            this.Grupo_IngresoGeneral.TabIndex = 0;
            this.Grupo_IngresoGeneral.TabStop = false;
            this.Grupo_IngresoGeneral.Text = "Ingreso general";
            // 
            // Etiqueta_Total
            // 
            this.Etiqueta_Total.AutoSize = true;
            this.Etiqueta_Total.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Total.Location = new System.Drawing.Point(6, 22);
            this.Etiqueta_Total.Name = "Etiqueta_Total";
            this.Etiqueta_Total.Size = new System.Drawing.Size(41, 19);
            this.Etiqueta_Total.TabIndex = 0;
            this.Etiqueta_Total.Text = "Total";
            // 
            // TextBox_TotalIVA
            // 
            this.TextBox_TotalIVA.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_TotalIVA.Location = new System.Drawing.Point(144, 85);
            this.TextBox_TotalIVA.Name = "TextBox_TotalIVA";
            this.TextBox_TotalIVA.ReadOnly = true;
            this.TextBox_TotalIVA.Size = new System.Drawing.Size(100, 27);
            this.TextBox_TotalIVA.TabIndex = 5;
            // 
            // Etiqueta_Neto
            // 
            this.Etiqueta_Neto.AutoSize = true;
            this.Etiqueta_Neto.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Neto.Location = new System.Drawing.Point(6, 55);
            this.Etiqueta_Neto.Name = "Etiqueta_Neto";
            this.Etiqueta_Neto.Size = new System.Drawing.Size(40, 19);
            this.Etiqueta_Neto.TabIndex = 2;
            this.Etiqueta_Neto.Text = "Neto";
            // 
            // Etiqueta_TotalIVA
            // 
            this.Etiqueta_TotalIVA.AutoSize = true;
            this.Etiqueta_TotalIVA.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_TotalIVA.Location = new System.Drawing.Point(6, 88);
            this.Etiqueta_TotalIVA.Name = "Etiqueta_TotalIVA";
            this.Etiqueta_TotalIVA.Size = new System.Drawing.Size(106, 19);
            this.Etiqueta_TotalIVA.TabIndex = 4;
            this.Etiqueta_TotalIVA.Text = "IVA acumulado";
            // 
            // TextBox_Neto
            // 
            this.TextBox_Neto.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Neto.Location = new System.Drawing.Point(144, 52);
            this.TextBox_Neto.Name = "TextBox_Neto";
            this.TextBox_Neto.ReadOnly = true;
            this.TextBox_Neto.Size = new System.Drawing.Size(100, 27);
            this.TextBox_Neto.TabIndex = 3;
            // 
            // TextBox_Total
            // 
            this.TextBox_Total.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Total.Location = new System.Drawing.Point(144, 19);
            this.TextBox_Total.Name = "TextBox_Total";
            this.TextBox_Total.ReadOnly = true;
            this.TextBox_Total.Size = new System.Drawing.Size(100, 27);
            this.TextBox_Total.TabIndex = 1;
            // 
            // Boton_Cerrar
            // 
            this.Boton_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Boton_Cerrar.Location = new System.Drawing.Point(187, 274);
            this.Boton_Cerrar.Name = "Boton_Cerrar";
            this.Boton_Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Boton_Cerrar.TabIndex = 2;
            this.Boton_Cerrar.Text = "Cerrar";
            this.Boton_Cerrar.UseVisualStyleBackColor = true;
            // 
            // Form_Ingresos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Boton_Cerrar;
            this.ClientSize = new System.Drawing.Size(274, 306);
            this.Controls.Add(this.Boton_Cerrar);
            this.Controls.Add(this.Grupo_IngresoFecha);
            this.Controls.Add(this.Grupo_IngresoGeneral);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Ingresos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ingresos";
            this.Grupo_IngresoFecha.ResumeLayout(false);
            this.Grupo_IngresoFecha.PerformLayout();
            this.Grupo_IngresoGeneral.ResumeLayout(false);
            this.Grupo_IngresoGeneral.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Grupo_IngresoFecha;
        private System.Windows.Forms.Label Etiqueta_Hoy;
        private System.Windows.Forms.TextBox TextBox_Mes;
        private System.Windows.Forms.Label Etiqueta_Mes;
        private System.Windows.Forms.TextBox TextBox_Hoy;
        private System.Windows.Forms.GroupBox Grupo_IngresoGeneral;
        private System.Windows.Forms.Label Etiqueta_Total;
        private System.Windows.Forms.TextBox TextBox_TotalIVA;
        private System.Windows.Forms.Label Etiqueta_Neto;
        private System.Windows.Forms.Label Etiqueta_TotalIVA;
        private System.Windows.Forms.TextBox TextBox_Neto;
        private System.Windows.Forms.TextBox TextBox_Total;
        private System.Windows.Forms.Button Boton_Cerrar;
        private System.Windows.Forms.Label Etiqueta_Anio;
        private System.Windows.Forms.TextBox TextBox_Anio;
    }
}