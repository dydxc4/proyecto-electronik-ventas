﻿using System;
using System.Windows.Forms;

namespace Ventas_Elektronik
{
    static class Programa
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form_Principal());
        }

        /// <summary>
        /// Administrador de datos.
        /// </summary>
        public static AdMon AdMon = new AdMon();
    }
}
