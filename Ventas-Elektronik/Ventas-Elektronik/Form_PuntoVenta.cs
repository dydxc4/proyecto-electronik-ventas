﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Ventas_Elektronik
{
    public partial class Form_PuntoVenta : Form
    {
        private readonly Venta venta = new Venta();
        private readonly AdMon admon = Programa.AdMon;

        #region Constructor

        /// <summary>
        /// Constructor principal para ventas nuevas.
        /// </summary>
        public Form_PuntoVenta()
        {
            InitializeComponent();
            venta = new Venta();
            CargarNuevaVenta();
        } 

        /// <summary>
        /// Constructor principal para mostrar una venta seleccionada.
        /// </summary>
        /// <param name="id">Identificador de la venta seleccionada.</param>
        public Form_PuntoVenta(int id)
        {
            InitializeComponent();
            venta = admon.Ventas[admon.ObtenerIndiceVenta(id)];
            TextBox_ID.Text = venta.ID.ToString();
            TextBox_Fecha.Text = venta.Fecha.ToString(admon.FormatoFecha);

            foreach (ArticuloVenta articulo in venta.Articulos)
            {
                AgregarArticulo(articulo);
            }

            ListView_Articulos.Update();
            ActualizarTotal();

            TextBox_Entrada.Enabled = false;
            Boton_ConcretarVenta.Enabled = false;
            Boton_Enter.Enabled = false;
            ModoVista = true;
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Crea una nueva venta.
        /// </summary>
        private void CargarNuevaVenta()
        {
            ListView_Articulos.Items.Clear();
            ListView_Articulos.Update();

            venta.Articulos.Clear();
            venta.ID = admon.GenerarIDVenta();

            TextBox_ID.Text = venta.ID.ToString();
            TextBox_Fecha.Text = venta.Fecha.ToString(Programa.AdMon.FormatoFecha);
            Text = $"Venta #{TextBox_ID.Text}";

            ActualizarTotal();
        }

        /// <summary>
        /// Procesa el texto ingresado y comprueba si los datos ingresados concuerdan con los existentes.
        /// </summary>
        private void ProcesarEntradaTexto()
        {
            string entrada = TextBox_Entrada.Text;
            string[] elementos;
            int id, indice;
            bool eliminarArticulo;

            try
            {
                if (!entrada.Contains('*')) // Cuando se ingresa un único ID.
                {
                    id = Convert.ToInt32(entrada);
                    eliminarArticulo = (id < 0);
                    id = Math.Abs(id);
                    indice = admon.ObtenerIndiceArticulo(id);

                    if (indice != -1)
                    {
                        if (eliminarArticulo) // Elimina
                        {
                            AdministrarArticulos(id, -1);
                        }
                        else // Agrega
                        {
                            AdministrarArticulos(id, 1);
                        }
                    }
                    else
                    {
                        MessageBox.Show("¡El artículo no existe!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else // Cuando se ingresa una cantidad e ID.
                {
                    elementos = entrada.Split('*');
                    id = Convert.ToInt32(elementos[1]);
                    indice = admon.ObtenerIndiceArticulo(id);

                    if (elementos.Length == 2 && indice != -1)
                    {
                        AdministrarArticulos(id , Convert.ToInt32(elementos[0]));
                    }
                    else
                    {
                        MessageBox.Show("¡El artículo no existe!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch
            {
                MessageBox.Show("¡Formato de entrada no válido!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            TextBox_Entrada.ResetText();
        }

        /// <summary>
        /// Administra los artículos en la venta.
        /// </summary>
        /// <param name="id">Identificador del artículo.</param>
        /// <param name="cantidad">Cantidad.</param>
        private void AdministrarArticulos(int id, int cantidad)
        {
            ArticuloVenta articulo;
            ListViewItem listViewitem;

            if (venta.ArticuloExiste(id))
            {
                foreach (ListViewItem elemento in ListView_Articulos.Items)
                {
                    if (elemento.Text == id.ToString())
                    {
                        listViewitem = elemento;
                        articulo = venta.Articulos[venta.ObtenerIndiceArticulo(id)];

                        int _cantidad = articulo.Cantidad + cantidad;

                        if (_cantidad <= 0)
                        {
                            venta.Articulos.Remove(articulo);
                            ListView_Articulos.Items.Remove(listViewitem);
                        }
                        else
                        {
                            articulo.Cantidad = _cantidad;
                            listViewitem.SubItems[2].Text = _cantidad.ToString();
                            listViewitem.SubItems[4].Text = articulo.ObtenerSubtotalConIVA().ToString(admon.FormatoMoneda);
                            listViewitem.SubItems[5].Text = articulo.ObtenerSubtotalSinIVA().ToString(admon.FormatoMoneda);
                        }
                        break;
                    }
                }
            }
            else if (cantidad > 0) // Si no existe se crea uno nuevo.
            {
                articulo = new ArticuloVenta(admon.Articulos[admon.ObtenerIndiceArticulo(id)], cantidad);
                AgregarArticulo(articulo);
                venta.Articulos.Add(articulo);
            }

            ListView_Articulos.Update();
            ActualizarTotal();
            TextBox_Entrada.ResetText();
        }

        /// <summary>
        /// Agrega un nuevo elemento al ListView
        /// </summary>
        private void AgregarArticulo(ArticuloVenta articulo)
        {
            string[] elementos = new string[6];

            elementos[0] = articulo.ID.ToString();
            elementos[1] = articulo.Nombre;
            elementos[2] = articulo.Cantidad.ToString();
            elementos[3] = articulo.ObtenerPrecioSinIVA().ToString(admon.FormatoMoneda); 
            elementos[4] = articulo.PrecioConIVA.ToString(admon.FormatoMoneda);
            elementos[5] = articulo.ObtenerSubtotalConIVA().ToString(admon.FormatoMoneda);

            ListView_Articulos.Items.Add(new ListViewItem(elementos));
        }

        /// <summary>
        /// Muestra el total de la compra.
        /// </summary>
        private void ActualizarTotal()
        {
            float _total, _importe;

            _importe = venta.ObtenerImporteVenta();
            _total = venta.ObtenerTotalVenta();

            TextBox_Importe.Text = _importe.ToString(admon.FormatoMoneda);
            TextBox_Total.Text = _total.ToString(admon.FormatoMoneda);
            TextBox_IVA.Text = (_total - _importe).ToString(admon.FormatoMoneda);
            Etiqueta_IVA.Text = $"IVA {admon.IVA}%";
            Etiqueta_Info.Text = $"{venta.Articulos.Count} artículos.";
        }

        /// <summary>
        /// Cancela la venta.
        /// </summary>
        private void CancelarVenta()
        {
            if (MessageBox.Show("¿Está seguro?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                CargarNuevaVenta();
            }
        }

        /// <summary>
        /// Concreta la venta.
        /// </summary>
        private void ConcretarVenta()
        {
            using (Form_TotalVenta formulario = new Form_TotalVenta(venta))
            {
                formulario.ShowDialog();
                Form_TotalVenta.Resultado resultado = formulario.ResultadoVenta;

                if (resultado == Form_TotalVenta.Resultado.Cancelar)
                {
                    CancelarVenta();
                } 
                else if (resultado == Form_TotalVenta.Resultado.Si)
                {
                    CargarNuevaVenta();
                }
            }
        }

        #endregion

        #region Propiedades

        /// <summary>
        /// Número de artículos.
        /// </summary>
        private int NumeroArticulos => venta.Articulos.Count;

        /// <summary>
        /// Modo de visualización; al ser verdadero deshabilita la función para ingresar más ventas.
        /// </summary>
        private bool ModoVista { get; set; } = false;

        #endregion

        #region Eventos

        /// <summary>
        /// Se produce cuando se presiona por primera vez una tecla.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_Entrada_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !string.IsNullOrWhiteSpace(TextBox_Entrada.Text)) // Tecla ENTER.
            {
                ProcesarEntradaTexto();
            }
            else if (e.KeyCode == Keys.Oemplus && venta.Articulos.Count > 0 && string.IsNullOrWhiteSpace(TextBox_Entrada.Text)) // Tecla '+'.
            {
                AdministrarArticulos(venta.Articulos[NumeroArticulos - 1].ID, 1);
            }
            else if (e.KeyCode == Keys.PageUp && venta.Articulos.Count > 0)
            {
                ConcretarVenta();
            }
            else if (e.KeyCode == Keys.PageDown && venta.Articulos.Count > 0)
            {
                CancelarVenta();
            } 
            else if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        /// <summary>
        /// Se produce al momento de presionar una tecla.
        /// </summary>
        private void TextBox_Entrada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '-' && e.KeyChar != '*')
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Se produce al presionar el botón principal.
        /// </summary>
        private void Boton_Enter_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TextBox_Entrada.Text))
            {
                ProcesarEntradaTexto();
            }
        }

        /// <summary>
        /// Se produce al presionar el botón concretar venta.
        /// </summary>
        private void Boton_ConcretarVenta_Click(object sender, EventArgs e)
        {
            if (venta.Articulos.Count > 0)
            {
                ConcretarVenta();
            }
        }

        /// <summary>
        /// Se produce al momento de intentar cerrar la venta, simpre y cuando se hayan seleccionado artículos.
        /// </summary>
        private void Form_Ventas_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (venta.Articulos.Count > 0 && !ModoVista)
            {
                if (MessageBox.Show("¿Está seguro?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Se produce cuando el formulario obtiene el foco.
        /// </summary>
        private void Form_Ventas_Activated(object sender, EventArgs e)
        {
            TextBox_Entrada.Focus();
        }

        #endregion
    }
}