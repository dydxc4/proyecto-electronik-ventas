﻿namespace Ventas_Elektronik
{
    partial class Form_Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Boton_IniciarVenta = new System.Windows.Forms.Button();
            this.Boton_Articulos = new System.Windows.Forms.Button();
            this.Boton_Ventas = new System.Windows.Forms.Button();
            this.Boton_Estadistica = new System.Windows.Forms.Button();
            this.Boton_Recargar = new System.Windows.Forms.Button();
            this.Etiqueta_Info = new System.Windows.Forms.Label();
            this.Boton_Cerrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Boton_Ajustes = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Boton_IniciarVenta
            // 
            this.Boton_IniciarVenta.Location = new System.Drawing.Point(11, 40);
            this.Boton_IniciarVenta.Name = "Boton_IniciarVenta";
            this.Boton_IniciarVenta.Size = new System.Drawing.Size(75, 52);
            this.Boton_IniciarVenta.TabIndex = 1;
            this.Boton_IniciarVenta.Text = "Iniciar punto de venta";
            this.Boton_IniciarVenta.UseVisualStyleBackColor = true;
            this.Boton_IniciarVenta.Click += new System.EventHandler(this.Boton_IniciarVenta_Click);
            // 
            // Boton_Articulos
            // 
            this.Boton_Articulos.Location = new System.Drawing.Point(173, 40);
            this.Boton_Articulos.Name = "Boton_Articulos";
            this.Boton_Articulos.Size = new System.Drawing.Size(75, 52);
            this.Boton_Articulos.TabIndex = 3;
            this.Boton_Articulos.Text = "Artículos";
            this.Boton_Articulos.UseVisualStyleBackColor = true;
            this.Boton_Articulos.Click += new System.EventHandler(this.Boton_Articulos_Click);
            // 
            // Boton_Ventas
            // 
            this.Boton_Ventas.Location = new System.Drawing.Point(92, 40);
            this.Boton_Ventas.Name = "Boton_Ventas";
            this.Boton_Ventas.Size = new System.Drawing.Size(75, 52);
            this.Boton_Ventas.TabIndex = 2;
            this.Boton_Ventas.Text = "Consulta de ventas";
            this.Boton_Ventas.UseVisualStyleBackColor = true;
            this.Boton_Ventas.Click += new System.EventHandler(this.Boton_Ventas_Click);
            // 
            // Boton_Estadistica
            // 
            this.Boton_Estadistica.Location = new System.Drawing.Point(11, 98);
            this.Boton_Estadistica.Name = "Boton_Estadistica";
            this.Boton_Estadistica.Size = new System.Drawing.Size(75, 52);
            this.Boton_Estadistica.TabIndex = 4;
            this.Boton_Estadistica.Text = "Información de ingresos";
            this.Boton_Estadistica.UseVisualStyleBackColor = true;
            this.Boton_Estadistica.Click += new System.EventHandler(this.Boton_Estadistica_Click);
            // 
            // Boton_Recargar
            // 
            this.Boton_Recargar.Location = new System.Drawing.Point(92, 98);
            this.Boton_Recargar.Name = "Boton_Recargar";
            this.Boton_Recargar.Size = new System.Drawing.Size(75, 52);
            this.Boton_Recargar.TabIndex = 5;
            this.Boton_Recargar.Text = "Recargar datos";
            this.Boton_Recargar.UseVisualStyleBackColor = true;
            this.Boton_Recargar.Click += new System.EventHandler(this.Boton_Recargar_Click);
            // 
            // Etiqueta_Info
            // 
            this.Etiqueta_Info.AutoSize = true;
            this.Etiqueta_Info.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Info.Location = new System.Drawing.Point(13, 172);
            this.Etiqueta_Info.Name = "Etiqueta_Info";
            this.Etiqueta_Info.Size = new System.Drawing.Size(0, 19);
            this.Etiqueta_Info.TabIndex = 8;
            // 
            // Boton_Cerrar
            // 
            this.Boton_Cerrar.Location = new System.Drawing.Point(172, 156);
            this.Boton_Cerrar.Name = "Boton_Cerrar";
            this.Boton_Cerrar.Size = new System.Drawing.Size(75, 52);
            this.Boton_Cerrar.TabIndex = 7;
            this.Boton_Cerrar.Text = "Cerrar";
            this.Boton_Cerrar.UseVisualStyleBackColor = true;
            this.Boton_Cerrar.Click += new System.EventHandler(this.Boton_Cerrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri Light", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bienvenido";
            // 
            // Boton_Ajustes
            // 
            this.Boton_Ajustes.Location = new System.Drawing.Point(172, 98);
            this.Boton_Ajustes.Name = "Boton_Ajustes";
            this.Boton_Ajustes.Size = new System.Drawing.Size(75, 52);
            this.Boton_Ajustes.TabIndex = 6;
            this.Boton_Ajustes.Text = "Ajustes";
            this.Boton_Ajustes.UseVisualStyleBackColor = true;
            this.Boton_Ajustes.Click += new System.EventHandler(this.Boton_Ajustes_Click);
            // 
            // Form_Principal
            // 
            this.AcceptButton = this.Boton_IniciarVenta;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 221);
            this.Controls.Add(this.Boton_Ajustes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Boton_Cerrar);
            this.Controls.Add(this.Etiqueta_Info);
            this.Controls.Add(this.Boton_Recargar);
            this.Controls.Add(this.Boton_Estadistica);
            this.Controls.Add(this.Boton_Ventas);
            this.Controls.Add(this.Boton_Articulos);
            this.Controls.Add(this.Boton_IniciarVenta);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ventas-Elektronik";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Boton_IniciarVenta;
        private System.Windows.Forms.Button Boton_Articulos;
        private System.Windows.Forms.Button Boton_Ventas;
        private System.Windows.Forms.Button Boton_Estadistica;
        private System.Windows.Forms.Button Boton_Recargar;
        private System.Windows.Forms.Label Etiqueta_Info;
        private System.Windows.Forms.Button Boton_Cerrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Boton_Ajustes;
    }
}