﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Ventas_Elektronik
{
    /// <summary>
    /// Almacena una “copia” de IVA cuando es llamada desde la propiedad IVA.
    /// </summary>
    class AdMon
    {
        private int _IVA = 8;

        /// <summary>
        /// Nombre del archivo de ajustes.
        /// </summary>
        private static readonly string NombreArchivo = "Ventas-Elektronik.ini";

        #region Constructor

        /// <summary>
        /// Constructor principal.
        /// </summary>
        public AdMon() { }

        /// <summary>
        /// Constructor principal.
        /// </summary>
        public AdMon(string rutaDatos)
        {
            RutaDatos = rutaDatos;
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Comprueba si el archivo de datos existe y llama a la función LeerDatos(), de lo contrario
        /// crea un nuevo archivo con los ejemplos y llama a la función LeerDatos(). 
        /// </summary>
        /// <returns>Devuelve si la lectura/creación funcionó.</returns>
        public bool Cargar()
        {
            bool estado = true;

            if (File.Exists(RutaDatos))
            {
                LeerDatos();
            }
            else
            {
                if (SeCargoAntes)
                {
                    MessageBox.Show("El archivo de datos a cambiado de ubicación o se ha eliminado.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    estado = false;
                }
                else
                {
                    MessageBox.Show($"Se procederá a crear un archivo con ejemplos en:\n {RutaDatos}.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (!CrearArchivo())
                    {
                        MessageBox.Show("No se pudo crear el archivo de ajustes.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        estado = false;
                    }
                }
            }
            return estado;
        }

        /// <summary>
        /// Lee los datos que se encuentran en el archivo de ajustes empezando por los artículos y luego 
        /// con ventas y ajustes. Cada artículo leído lo va almacenando en la propiedad Articulos; 
        /// cada venta en Ventas. En caso de encontrar un error a la hora de leer un dato, este es omitido.
        /// </summary>
        private void LeerDatos()
        {
            string linea;
            string[] sublinea;
            Articulos = new List<Articulo>();
            Ventas = new List<Venta>();
            int id;

            using (StreamReader lector = File.OpenText(RutaDatos)) // Primero procesa los artículos.
            {
                while ((linea = lector.ReadLine()) != null)
                {
                    if (linea.ToUpper().StartsWith(Articulo.InicioLinea)) // Procesa un artículo.
                    {
                        try
                        {
                            sublinea = linea.Substring(Articulo.InicioLinea.Length).Split(';');
                            if (sublinea.Length == 3)
                            {
                                id = Convert.ToInt32(sublinea[0]);

                                if (id != 0) // El ID debe diferir de cero.
                                {
                                    Articulos.Add(new Articulo
                                    {
                                        ID = id, // ID
                                        Nombre = sublinea[1], // Nombre
                                        PrecioConIVA = Convert.ToSingle(sublinea[2]) // Precio
                                    });
                                }
                            }
                        }
                        catch { }
                    }
                }
            }

            using (StreamReader lector = File.OpenText(RutaDatos)) // Luego las ventas.
            {
                while ((linea = lector.ReadLine()) != null)
                {
                    if (linea.ToUpper().StartsWith(Venta.InicioLinea)) // Ventas.
                    {
                        try
                        {
                            sublinea = linea.Substring(Venta.InicioLinea.Length).Split(';');
                            if (sublinea.Length > 2)
                            {
                                id = Convert.ToInt32(sublinea[0]);
                                if (id != 0)
                                {
                                    Venta venta = new Venta
                                    {
                                        ID = id,
                                        Fecha = Convert.ToDateTime(sublinea[1])
                                    };

                                    string[] subsublinea;

                                    for (int i = 2; i < sublinea.Length; i++)
                                    {
                                        subsublinea = sublinea[i].Remove(sublinea[i].Length - 1, 1).Split('(', '*');

                                        if (subsublinea.Length == 3)
                                        {
                                            int indice, idArticulo = Convert.ToInt32(subsublinea[0]);
                                            indice = ObtenerIndiceArticulo(idArticulo);
                                            venta.Articulos.Add(new ArticuloVenta
                                            {
                                                ID = idArticulo,
                                                Nombre = indice != -1 ? Articulos[indice].Nombre : "Artículo desconocido",
                                                Cantidad = Convert.ToInt32(subsublinea[1]),
                                                PrecioConIVA = Convert.ToSingle(subsublinea[2])
                                            });
                                        }
                                    }
                                    Ventas.Add(venta);
                                }
                            }
                        }
                        catch { }
                    }
                    if (linea.ToUpper().StartsWith("IVA=")) // IVA
                    {
                        try
                        {
                            _IVA = Convert.ToInt32(linea.Substring(4, 1));
                        }
                        catch { }
                    }
                }
                lector.Close();
            }
            if (!SeCargoAntes) { SeCargoAntes = true; } // Se ejecuta una sola vez.
        }

        #endregion

        #region Propiedades

        /// <summary>
        /// Auxiliar: Indica si los datos se habían cargado anteriormente.
        /// </summary>
        private bool SeCargoAntes { get; set; } = false;

        /// <summary>
        /// Lista de artículos registrados.
        /// </summary>
        public List<Articulo> Articulos { get; private set; } = new List<Articulo>();

        /// <summary>
        /// Lista de ventas registradas.
        /// </summary>
        public List<Venta> Ventas { get; private set; } = new List<Venta>();

        /// <summary>
        /// Impuesto de valor agregado. Por defecto es del 8%.
        /// </summary>
        public int IVA
        {
            get => _IVA;
            set
            {
                if (value > 0 && value < 100)
                {
                    ActualizarAjuste($"IVA={_IVA}", $"IVA={value}");
                }
            }
        }

        /// <summary>
        /// Ruta del archivo de ajustes.
        /// </summary>
        public string RutaDatos { get; set; } = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\{NombreArchivo}";

        /// <summary>
        /// Formato para fechas.
        /// </summary>
        public string FormatoFecha { get; } = "dd/MM/yyyy";

        /// <summary>
        /// Formato para moneda.
        /// </summary>
        public string FormatoMoneda { get; } = "C";

        /// <summary>
        /// Formato para identificadores.
        /// </summary>
        public string FormatoID { get; } = "D6";

        #endregion

        #region Funciones principales

        /// <summary>
        /// Crea un nuevo archivo de datos.
        /// </summary>
        private bool CrearArchivo()
        {
            try
            {
                File.WriteAllText(RutaDatos,
                    "# LÍNEAS QUE EMPIECEN CON '#' REPRESENTAN UN COMENTARIO.\n" +
                    "#\n" +
                    "# [ESTRUCTURA]\n" +
                    "# Venta:\n" +
                    "# V=[S:N];ID_VENTA;FECHA;ID_ARTICULO(CANTIDAD*PRECIO)[;ID_ARTICULO_2(CANTIDAD*PRECIO)]\n" +
                    "#\n" +
                    "# [S:N]			Indica si la venta se concreto o no.\n" +
                    "# ID_VENTA		Número de venta conformado por 6 dígitos.\n" +
                    $"# FECHA			Fecha en formato {FormatoFecha} (Ejemplo: 24/11/2019).\n" +
                    "# ID_ARTICULO	Identificador del artículo.\n" +
                    "# CANTIDAD		Cantidad de productos.\n" +
                    "# PRECIO		Precio del productos, admite decimales.\n" +
                    "#\n" +
                    "# Artículo:\n" +
                    "# A=ID_ARTICULO;NOMBRE;PRECIO\n" +
                    "#\n" +
                    "# ID_ARTICULO	Identificador del producto.\n" +
                    "# NOMBRE		Nombre del artículo.\n" +
                    "# PRECIO		Precio del producto, admite decimales.\n" +
                    "#\n" +
                    "# IMPORTANTE: El ID debe diferir de cero.\n\n" +
                    "IVA=8\n\n" +
                    "A=000001;Protoboard 830 puntos;120\n" +
                    "A=000002;Display 7 segmentos 1 dígito cátodo común;22\n" +
                    "A=000003;Display LCD 1602 Azul i2c;89\n" +
                    "A=000004;Sensor de gas MQ-2;89\n" +
                    "A=000005;MicroSD 32GB;290\n" +
                    "A=000006;LED Cualquier color 5mm;2.5\n" +
                    "A=000007;Resistencia cualquier valor 1/4W;0.5\n" +
                    "A=000008;Resistencia LDR;5\n" +
                    "A=000009;Diodo Zener;5\n" +
                    "A=000010;Arduino UNO R3;450\n" +
                    "A=000011;Cable 1m;4.5\n" +
                    "A=000012;Sensor distancia HC-SR04;75\n" +
                    "A=000013;Servomotor SG90;78\n" +
                    "A=000014;Motor paso por paso;59\n" +
                    "A=000015;Sensor de temperatura y humedad;90\n"
                ); ;
                LeerDatos();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Registra una nueva actualización.
        /// </summary>
        /// <param name="contenido">Contenido que se anexará.</param>
        public bool AgregarNuevoElemento(string contenido)
        {
            bool correcto = false;
            try
            {
                if (File.Exists(RutaDatos))
                {
                    using (StreamWriter streamWriter = File.AppendText(RutaDatos))
                    {
                        streamWriter.WriteLine(contenido);
                        streamWriter.Close();
                        correcto = Cargar();
                    }
                }
            }
            catch { }
            return correcto;
        }

        /// <summary>
        /// Edita un artículo existente.
        /// </summary>
        /// <param name="id">Id. de artículo.</param>
        /// <param name="nuevoArticulo">Nuevo artículo.</param>
        public bool EditarArticulo(int id, Articulo nuevoArticulo)
        {
            bool estado = false;
            int indice = Programa.AdMon.ObtenerIndiceArticulo(id);

            if (indice != -1)
            {
                estado = ActualizarAjuste(Articulos[indice].ToString(), nuevoArticulo.ToString());

            }
            return estado;
        }

        /// <summary>
        /// Actualiza un valor en el archivo.
        /// </summary>
        /// <param name="valorAnterior">Valor anterior.</param>
        /// <param name="valorNuevo">Nuevo valor.</param>
        public bool ActualizarAjuste(string valorAnterior, string valorNuevo)
        {
            bool estado = false;
            try
            {
                string contenido = File.ReadAllText(RutaDatos);
                contenido = contenido.Replace(valorAnterior, valorNuevo);
                File.WriteAllText(RutaDatos, contenido);
                estado = Cargar();
            }
            catch { }

            return estado;
        }

        /// <summary>
        /// Eliminar el artículo en base a su identificador.
        /// </summary>
        /// <param name="id">Id. del artículo.</param>
        public bool EliminarArticulo(int id)
        {
            bool correcto = false;
            int indice = Programa.AdMon.ObtenerIndiceArticulo(id);

            if (indice != 0)
            {
                try
                {
                    string contenido = File.ReadAllText(RutaDatos);
                    contenido = contenido.Replace(Articulos[indice].ToString(), "");
                    File.WriteAllText(RutaDatos, contenido);
                    correcto = Cargar();
                }
                catch { }
            }
            return correcto;
        }

        /// <summary>
        /// Obtiene el índice correspondiente al identificador del artículo ingresado.
        /// </summary>
        /// <param name="id">Identificador de artículo.</param>
        /// <returns>Número de índice, -1 si no existe.</returns>
        public int ObtenerIndiceArticulo(int id)
        {
            int valor = -1;

            for (int i = 0; i < Articulos.Count; i++)
            {
                if (Articulos[i].ID == id)
                {
                    valor = i;
                    break;
                }
            }
            return valor;
        }

        /// <summary>
        /// Obtiene el índice correspondiente al identificador de la venta ingresada.
        /// </summary>
        /// <param name="id">Identificador de artículo.</param>
        /// <returns>Número de índice, -1 si no existe.</returns>
        public int ObtenerIndiceVenta(int id)
        {
            int valor = -1;
            for (int i = 0; i < Ventas.Count; i++)
            {
                if (Ventas[i].ID == id)
                {
                    valor = i;
                    break;
                }
            }
            return valor;
        }

        /// <summary>
        /// Genera un nuevo identificador para un nuevo artículo.
        /// </summary>
        /// <returns>Identificador de artículo.</returns>
        public int GenerarIDArticulo()
        {
            int valorMayor = 0;
            for (int i = 0; i < Articulos.Count; i++)
            {
                if (Articulos[i].ID > valorMayor)
                {
                    valorMayor = Articulos[i].ID;
                }
            }
            return valorMayor + 1;
        }

        /// <summary>
        /// Genera un nuevo identificador para una nueva venta.
        /// </summary>
        /// <returns>Identificador de venta.</returns>
        public int GenerarIDVenta()
        {
            int valorMayor = 0;
            for (int i = 0; i < Ventas.Count; i++)
            {
                if (Ventas[i].ID > valorMayor)
                {
                    valorMayor = Ventas[i].ID;
                }
            }
            return valorMayor + 1;
        }

        /// <summary>
        /// Devuelve un valor sin el IVA agregado.
        /// </summary>
        /// <param name="valorInicial">Valor al que se desea sumar el IVA.</param>
        public float ObtenerValorSinIVA(float valorInicial)
        {
            return valorInicial - (0.01F * IVA * valorInicial);
        }

        #endregion
    }
}
