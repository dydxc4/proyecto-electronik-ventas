﻿using System;
using System.Collections.Generic;

namespace Ventas_Elektronik
{
    /// <summary>
    /// Almacena las propiedades de una venta.
    /// </summary>
    public class Venta
    {
        /// <summary>
        /// Constructor principal.
        /// </summary>
        public Venta() { }

        #region Propiedades

        public int ID { get; set; } = 0;
        public DateTime Fecha { get; set; } = DateTime.Now;

        /// <summary>
        /// Lista de artículos.
        /// </summary>
        public List<ArticuloVenta> Articulos { get; set; } = new List<ArticuloVenta>();

        /// <summary>
        /// Inicio de línea en el archivo de configuraciones.
        /// </summary>
        public static string InicioLinea => "V=";

        #endregion

        #region Funciones

        /// <summary>
        /// Devuelve el importe de la venta (la suma de los precios sin IVA).
        /// </summary>
        public float ObtenerImporteVenta()
        {
            return Programa.AdMon.ObtenerValorSinIVA(ObtenerTotalVenta());
        }

        /// <summary>
        /// Devuelve el total de la venta (con IVA incluido).
        /// </summary>
        public float ObtenerTotalVenta()
        {
            float _total = 0;
            foreach (ArticuloVenta articulo in Articulos)
            {
                _total += articulo.ObtenerSubtotalConIVA();
            }
            return _total;
        }

        /// <summary>
        /// Devuelve el importe de IVA de la venta (lo que se pagará de impuestos).
        /// </summary>
        public float ObtenerIVAVenta()
        {
            return ObtenerTotalVenta() - ObtenerImporteVenta();
        }

        /// <summary>
        /// Comprueba si el id del artículo correspondiente esta presente en la compra.
        /// </summary>
        /// <param name="id">Id. del artículo.</param>
        public bool ArticuloExiste(int id)
        {
            bool resultado = false;

            foreach (ArticuloVenta elemento in Articulos)
            {
                if (elemento.ID == id)
                {
                    resultado = true;
                }
            }

            return resultado;
        }

        /// <summary>
        /// Devuelve la posición del artículo en la lista.
        /// </summary>
        /// <param name="id">Identificador del artículo.</param>
        public int ObtenerIndiceArticulo(int id)
        {
            int resultado = -1;

            for (int i = 0; i < Articulos.Count; i++)
            {
                if (Articulos[i].ID == id)
                {
                    resultado = i;
                }
            }

            return resultado;
        }

        /// <summary>
        /// Devuelve el contenido de la compra en una línea para ser insertada directamente en el archivo de datos.
        /// </summary>
        /// <returns>Valor formateado.</returns>
        public override string ToString()
        {
            string cadena = $"{InicioLinea}{ID.ToString(Programa.AdMon.FormatoID)};{Fecha.ToString(Programa.AdMon.FormatoFecha)}";

            foreach (ArticuloVenta articulo in Articulos)
            {
                cadena += $";{articulo.ToString()}";
            }
            return cadena;
        }

        #endregion

    }

    /// <summary>
    /// Establece los datos referentes a un artículo.
    /// </summary>
    public class Articulo
    {
        /// <summary>
        /// Constructor principal.
        /// </summary>
        public Articulo() { }

        /// <summary>
        /// Constructor principal. Auxiliar en la copia de artículos.
        /// </summary>
        /// <param name="articulo">Artículo original.</param>
        public Articulo(Articulo articulo)
        {
            ID = articulo.ID;
            Nombre = articulo.Nombre;
            PrecioConIVA = articulo.PrecioConIVA;
        }

        public int ID { get; set; } = 0;
        public string Nombre { get; set; } = "Artículo";

        /// <summary>
        /// Precio sin IVA.
        /// </summary>
        public float PrecioConIVA { get; set; } = 0;

        /// <summary>
        /// Inicio de línea en el archivo de configuraciones.
        /// </summary>
        public static string InicioLinea => "A=";

        /// <summary>
        /// Devuelve el contenido del artículo en una línea para ser insertada directamente en el archivo de datos.
        /// </summary>
        /// <returns>Valor formateado.</returns>
        public override string ToString()
        {
            return $"{InicioLinea}{ID.ToString(Programa.AdMon.FormatoID)};{Nombre};{PrecioConIVA}";
        }

        /// <summary>
        /// Devuelve el precio original más el IVA.
        /// </summary>
        public float ObtenerPrecioSinIVA()
        {
            return Programa.AdMon.ObtenerValorSinIVA(PrecioConIVA);
        }
    }

    /// <summary>
    /// Establece los datos referentes a un artículo en una venta.
    /// </summary>
    public class ArticuloVenta : Articulo
    {
        /// <summary>
        /// Constructor principal.
        /// </summary>
        public ArticuloVenta() { }

        /// <summary>
        /// Constructor principal.
        /// </summary>
        /// <param name="articulo">Artículo original.</param>
        /// <param name="cantidad">Número de unidades.</param>
        public ArticuloVenta(Articulo articulo, int cantidad)
        {
            ID = articulo.ID;
            Nombre = articulo.Nombre;
            PrecioConIVA = articulo.PrecioConIVA;
            Cantidad = cantidad;
        }

        public int Cantidad { get; set; } = 1;

        /// <summary>
        /// Devuelve el contenido del artículo en una línea para ser insertada directamente en el archivo de datos.
        /// </summary>
        public override string ToString()
        {
            return $"{ID.ToString(Programa.AdMon.FormatoID)}({Cantidad}*{PrecioConIVA})";
        }

        /// <summary>
        /// Devuelve el subtotal del artículo con IVA.
        /// </summary>
        public float ObtenerSubtotalConIVA()
        {
            return Cantidad * PrecioConIVA;
        }

        /// <summary>
        /// Devuelve el subtotal del artículo sin IVA.
        /// </summary>
        public float ObtenerSubtotalSinIVA()
        {
            return Programa.AdMon.ObtenerValorSinIVA(ObtenerSubtotalConIVA());
        }
    }
}
