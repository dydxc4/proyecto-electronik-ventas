﻿using System;
using System.Windows.Forms;

namespace Ventas_Elektronik
{
    public partial class Form_Articulos : Form
    {
        private readonly AdMon adMon = Programa.AdMon;
        private int cantidadArticulos;

        #region Constructor

        /// <summary>
        /// Constructor principal.
        /// </summary>
        public Form_Articulos()
        {
            InitializeComponent();
            CargarElementosListView();
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Carga los artículos dentro de un ListView.
        /// </summary>
        private void CargarElementosListView()
        {
            cantidadArticulos = 0;
            ListView_Articulos.Items.Clear();
            foreach (Articulo articulo in adMon.Articulos)
            {
                if (articulo.ID != 0)
                {
                    string[] elemento = new string[4];
                    elemento[0] = articulo.ID.ToString();
                    elemento[1] = articulo.Nombre;
                    elemento[2] = articulo.ObtenerPrecioSinIVA().ToString(Programa.AdMon.FormatoMoneda);
                    elemento[3] = articulo.PrecioConIVA.ToString(Programa.AdMon.FormatoMoneda);

                    ListView_Articulos.Items.Add(new ListViewItem(elemento));
                    cantidadArticulos++;
                }
            }
            ListView_Articulos.Update();
            Etiqueta_CantidadArticulos.Text = $"{cantidadArticulos} artículos registrados.";
        }

        /// <summary>
        /// Abre la venta de edición.
        /// </summary>
        private void EditarSeleccion()
        {
            if (ListView_Articulos.SelectedItems.Count != 0)
            {
                using (Form_EdicionArticulo form = new Form_EdicionArticulo(ObtenerIDSeleccion()))
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        CargarElementosListView();
                    }
                }
            }
        }

        #endregion

        #region Funciones

        /// <summary>
        /// Obtiene el identificador del artículo correspondiente al elemento del ListView seleccionado.
        /// </summary>
        private int ObtenerIDSeleccion()
        {
            return Convert.ToInt32(ListView_Articulos.SelectedItems[0].Text);
        }

        #endregion

        #region Eventos

        /// <summary>
        /// Tiene lugar cuando se presiona el botón "Nuevo elemento".
        /// </summary>
        private void Boton_Nuevo_Click(object sender, EventArgs e)
        {
            using (Form_EdicionArticulo form = new Form_EdicionArticulo())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    CargarElementosListView();
                }
            }
        }

        /// <summary>
        /// Tiene lugar cuando se presiona el botón "Editar elemento".
        /// </summary>
        private void Boton_Editar_Click(object sender, EventArgs e)
        {
            EditarSeleccion();
        }

        /// <summary>
        /// Tiene lugar cuando se presiona el botón "Eliminar elemento".
        /// </summary>
        private void Boton_Eliminar_Click(object sender, EventArgs e)
        {
            if (ListView_Articulos.SelectedItems.Count != 0)
            {
                if (MessageBox.Show("¿Está seguro?", "Confirmar eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    adMon.EliminarArticulo(ObtenerIDSeleccion());
                    CargarElementosListView();
                }
            }
        }

        /// <summary>
        /// Tiene lugar cuando se da doble clic en un elemento del ListView principal.
        /// </summary>
        private void ListView_Articulos_DoubleClick(object sender, EventArgs e)
        {
            EditarSeleccion();
        }

        #endregion

    }
}