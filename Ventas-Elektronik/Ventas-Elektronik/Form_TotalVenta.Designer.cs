﻿namespace Ventas_Elektronik
{
    partial class Form_TotalVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Etiqueta_Total = new System.Windows.Forms.Label();
            this.Etiqueta_Pago = new System.Windows.Forms.Label();
            this.TextBox_Total = new System.Windows.Forms.TextBox();
            this.TextBox_Pago = new System.Windows.Forms.TextBox();
            this.Boton_Continuar = new System.Windows.Forms.Button();
            this.Boton_Volver = new System.Windows.Forms.Button();
            this.Etiqueta_Cambio = new System.Windows.Forms.Label();
            this.TextBox_Cambio = new System.Windows.Forms.TextBox();
            this.Boton_Cancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Etiqueta_Total
            // 
            this.Etiqueta_Total.AutoSize = true;
            this.Etiqueta_Total.Font = new System.Drawing.Font("Calibri Light", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Total.Location = new System.Drawing.Point(12, 15);
            this.Etiqueta_Total.Name = "Etiqueta_Total";
            this.Etiqueta_Total.Size = new System.Drawing.Size(57, 28);
            this.Etiqueta_Total.TabIndex = 0;
            this.Etiqueta_Total.Text = "Total";
            // 
            // Etiqueta_Pago
            // 
            this.Etiqueta_Pago.AutoSize = true;
            this.Etiqueta_Pago.Font = new System.Drawing.Font("Calibri Light", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Pago.Location = new System.Drawing.Point(12, 56);
            this.Etiqueta_Pago.Name = "Etiqueta_Pago";
            this.Etiqueta_Pago.Size = new System.Drawing.Size(57, 28);
            this.Etiqueta_Pago.TabIndex = 0;
            this.Etiqueta_Pago.Text = "Pago";
            // 
            // TextBox_Total
            // 
            this.TextBox_Total.Font = new System.Drawing.Font("Calibri", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Total.Location = new System.Drawing.Point(112, 12);
            this.TextBox_Total.Name = "TextBox_Total";
            this.TextBox_Total.ReadOnly = true;
            this.TextBox_Total.Size = new System.Drawing.Size(156, 35);
            this.TextBox_Total.TabIndex = 0;
            // 
            // TextBox_Pago
            // 
            this.TextBox_Pago.Font = new System.Drawing.Font("Calibri", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Pago.Location = new System.Drawing.Point(112, 53);
            this.TextBox_Pago.Name = "TextBox_Pago";
            this.TextBox_Pago.Size = new System.Drawing.Size(156, 35);
            this.TextBox_Pago.TabIndex = 1;
            this.TextBox_Pago.TextChanged += new System.EventHandler(this.TextBox_Pago_TextChanged);
            this.TextBox_Pago.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_Pago_KeyDown);
            this.TextBox_Pago.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_Pago_KeyPress);
            // 
            // Boton_Continuar
            // 
            this.Boton_Continuar.Enabled = false;
            this.Boton_Continuar.Location = new System.Drawing.Point(193, 135);
            this.Boton_Continuar.Name = "Boton_Continuar";
            this.Boton_Continuar.Size = new System.Drawing.Size(75, 23);
            this.Boton_Continuar.TabIndex = 3;
            this.Boton_Continuar.Text = "Continuar";
            this.Boton_Continuar.UseVisualStyleBackColor = true;
            this.Boton_Continuar.Click += new System.EventHandler(this.Boton_Continuar_Click);
            // 
            // Boton_Volver
            // 
            this.Boton_Volver.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Boton_Volver.Location = new System.Drawing.Point(31, 135);
            this.Boton_Volver.Name = "Boton_Volver";
            this.Boton_Volver.Size = new System.Drawing.Size(75, 23);
            this.Boton_Volver.TabIndex = 1;
            this.Boton_Volver.Text = "Volver";
            this.Boton_Volver.UseVisualStyleBackColor = true;
            // 
            // Etiqueta_Cambio
            // 
            this.Etiqueta_Cambio.AutoSize = true;
            this.Etiqueta_Cambio.Font = new System.Drawing.Font("Calibri Light", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etiqueta_Cambio.Location = new System.Drawing.Point(12, 97);
            this.Etiqueta_Cambio.Name = "Etiqueta_Cambio";
            this.Etiqueta_Cambio.Size = new System.Drawing.Size(82, 28);
            this.Etiqueta_Cambio.TabIndex = 0;
            this.Etiqueta_Cambio.Text = "Cambio";
            // 
            // TextBox_Cambio
            // 
            this.TextBox_Cambio.Font = new System.Drawing.Font("Calibri", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_Cambio.Location = new System.Drawing.Point(112, 94);
            this.TextBox_Cambio.Name = "TextBox_Cambio";
            this.TextBox_Cambio.ReadOnly = true;
            this.TextBox_Cambio.Size = new System.Drawing.Size(156, 35);
            this.TextBox_Cambio.TabIndex = 0;
            // 
            // Boton_Cancelar
            // 
            this.Boton_Cancelar.Location = new System.Drawing.Point(112, 135);
            this.Boton_Cancelar.Name = "Boton_Cancelar";
            this.Boton_Cancelar.Size = new System.Drawing.Size(75, 23);
            this.Boton_Cancelar.TabIndex = 2;
            this.Boton_Cancelar.Text = "Cancelar";
            this.Boton_Cancelar.UseVisualStyleBackColor = true;
            this.Boton_Cancelar.Click += new System.EventHandler(this.Boton_Cancelar_Click);
            // 
            // Form_TotalVenta
            // 
            this.AcceptButton = this.Boton_Continuar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Boton_Volver;
            this.ClientSize = new System.Drawing.Size(280, 170);
            this.Controls.Add(this.Boton_Cancelar);
            this.Controls.Add(this.TextBox_Cambio);
            this.Controls.Add(this.Etiqueta_Cambio);
            this.Controls.Add(this.Boton_Volver);
            this.Controls.Add(this.Boton_Continuar);
            this.Controls.Add(this.TextBox_Pago);
            this.Controls.Add(this.TextBox_Total);
            this.Controls.Add(this.Etiqueta_Pago);
            this.Controls.Add(this.Etiqueta_Total);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_TotalVenta";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TotalVenta";
            this.Activated += new System.EventHandler(this.Form_TotalVenta_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Etiqueta_Total;
        private System.Windows.Forms.Label Etiqueta_Pago;
        private System.Windows.Forms.TextBox TextBox_Total;
        private System.Windows.Forms.TextBox TextBox_Pago;
        private System.Windows.Forms.Button Boton_Continuar;
        private System.Windows.Forms.Button Boton_Volver;
        private System.Windows.Forms.Label Etiqueta_Cambio;
        private System.Windows.Forms.TextBox TextBox_Cambio;
        private System.Windows.Forms.Button Boton_Cancelar;
    }
}