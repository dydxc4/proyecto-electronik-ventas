﻿using System;
using System.Windows.Forms;

namespace Ventas_Elektronik
{
    public partial class Form_TotalVenta : Form
    {
        public enum Resultado { Si, No, Cancelar };

        private float total, pago = 0;
        private readonly Venta _venta;

        /// <summary>
        /// Constructor principal.
        /// </summary>
        /// <param name="venta">Venta que intentará concretar.</param>
        public Form_TotalVenta(Venta venta)
        {
            InitializeComponent();
            _venta = venta;
            total = _venta.ObtenerTotalVenta();
            TextBox_Total.Text = total.ToString(Programa.AdMon.FormatoMoneda);
            ActualizarCambio();
        }

        #region Métodos

        /// <summary>
        /// Actualiza el cambio.
        /// </summary>
        private void ActualizarCambio()
        {
            float _cambio = 0;
            try
            {
                if (!string.IsNullOrWhiteSpace(TextBox_Pago.Text))
                {
                    pago = Convert.ToSingle(TextBox_Pago.Text);
                    _cambio = pago - total;
                    Boton_Continuar.Enabled = _cambio >= 0;
                }
            }
            catch { }
            TextBox_Cambio.Text = _cambio.ToString(Programa.AdMon.FormatoMoneda);
        }

        /// <summary>
        /// Guarda la venta en el archivo.
        /// </summary>
        private void ConcretarVenta()
        {
            bool correcto = Programa.AdMon.AgregarNuevoElemento(_venta.ToString());
            if (correcto)
            {
                ResultadoVenta = Resultado.Si;
            }
            else
            {
                ResultadoVenta = Resultado.No;
            }
            Close();
        }

        #endregion

        /// <summary>
        /// Indica si la venta se concreto.
        /// </summary>
        public Resultado ResultadoVenta { get; private set; } = Resultado.No;

        #region Eventos.

        /// <summary>
        /// Impide que se ingrese algo diferente a números.
        /// </summary>
        private void TextBox_Pago_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Se produce cuando se cambia de valor.
        /// </summary>
        private void TextBox_Pago_TextChanged(object sender, EventArgs e)
        {
            ActualizarCambio();
        }

        /// <summary>
        /// Se produce cuando se presiona una tecla.
        /// </summary>
        private void TextBox_Pago_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && pago >= total) // Tecla ENTER.
            {
                ConcretarVenta();
            }
        }

        /// <summary>
        /// Se produce cuando se presiona el botón "Cancelar".
        /// </summary>
        private void Boton_Cancelar_Click(object sender, EventArgs e)
        {
            ResultadoVenta = Resultado.Cancelar;
            Close();
        }

        /// <summary>
        /// Se produce cuando el formulario obtiene el foco de atención.
        /// </summary>
        private void Form_TotalVenta_Activated(object sender, EventArgs e)
        {
            TextBox_Pago.Focus();
        }

        /// <summary>
        /// Se produce al presionar el botón "Continuar".
        /// </summary>
        private void Boton_Continuar_Click(object sender, EventArgs e)
        {
            if (pago >= total)
            {
                ConcretarVenta();
            }
        }

        #endregion
    }
}
