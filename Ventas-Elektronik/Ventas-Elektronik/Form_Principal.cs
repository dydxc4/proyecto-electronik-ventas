﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace Ventas_Elektronik
{
    public partial class Form_Principal : Form
    {
        /// <summary>
        /// Constructor principal.
        /// </summary>
        public Form_Principal()
        {
            InitializeComponent();
            Cargar();
        }

        /// <summary>
        /// Carga los datos.
        /// </summary>
        private void Cargar()
        {
            if (Programa.AdMon.Cargar())
            {
                Etiqueta_Info.Text = "OK.";
                Etiqueta_Info.ForeColor = Color.Green;
            }
            else
            {
                Etiqueta_Info.Text = "Error.";
                Etiqueta_Info.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// Muestra el formulario.
        /// </summary>
        private void Ocultar()
        {
            ShowInTaskbar = false;
            Hide();
        }

        /// <summary>
        /// Oculta el formulario.
        /// </summary>
        private void Mostrar()
        {
            ShowInTaskbar = true;
            Show();
        }

        #region Eventos

        /// <summary>
        /// Se produce cuando se presiona el botón "Recargar".
        /// </summary>
        private void Boton_Recargar_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        /// <summary>
        /// Ventana artículos.
        /// </summary>
        private void Boton_Articulos_Click(object sender, EventArgs e)
        {
            using (Form_Articulos form = new Form_Articulos())
            {
                Ocultar();
                form.ShowDialog();
            }
            Mostrar();
        }

        /// <summary>
        /// Ventana punto de venta.
        /// </summary>
        private void Boton_IniciarVenta_Click(object sender, EventArgs e)
        {
            using (Form_PuntoVenta form = new Form_PuntoVenta())
            {
                Ocultar();
                form.ShowDialog();
            }
            Mostrar();
        }

        /// <summary>
        /// Ventana ventas.
        /// </summary>
        private void Boton_Ventas_Click(object sender, EventArgs e)
        {
            using (Form_ConsultaVentas form = new Form_ConsultaVentas())
            {
                Ocultar();
                form.ShowDialog();
            }
            Mostrar();
        }

        /// <summary>
        /// Ventana estadística.
        /// </summary>
        private void Boton_Estadistica_Click(object sender, EventArgs e)
        {
            using (Form_Ingresos form = new Form_Ingresos())
            {
                Ocultar();
                form.ShowDialog();
            }
            Mostrar();
        }

        /// <summary>
        /// Abre el formulario de ajustes.
        /// </summary>
        private void Boton_Ajustes_Click(object sender, EventArgs e)
        {
            using (Form_Ajustes form = new Form_Ajustes())
            {
                form.ShowDialog();
            }
        }

        /// <summary>
        /// Cierra el formulario.
        /// </summary>
        private void Boton_Cerrar_Click(object sender, EventArgs e)
        {            
            Close();
        }

        #endregion
    }
}
