﻿using System;
using System.Windows.Forms;

namespace Ventas_Elektronik
{
    public partial class Form_EdicionArticulo : Form
    {
        private readonly Articulo _articulo = new Articulo();

        #region Constructores

        /// <summary>
        /// Nuevo artículo.
        /// </summary>
        public Form_EdicionArticulo()
        {
            _articulo.ID = Programa.AdMon.GenerarIDArticulo();
            Cargar();
        }

        /// <summary>
        /// Edición de artículo.
        /// </summary>
        /// <param name="id">Id. del artículo que se va a editar.</param>
        public Form_EdicionArticulo(int id)
        {
            _articulo = new Articulo(Programa.AdMon.Articulos[Programa.AdMon.ObtenerIndiceArticulo(id)]);
            ModoEdicion = true;
            Cargar();
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Carga las propiedades del artículo.
        /// </summary>
        private void Cargar()
        {
            InitializeComponent();
            Etiqueta_IVA.Text = $"IVA {Programa.AdMon.IVA}%";
            PrecioConIVA = _articulo.PrecioConIVA;
            TextBox_ID.Text = _articulo.ID.ToString();
            TextBox_Nombre.Text = _articulo.Nombre;
            TextBox_PrecioConIVA.Text = PrecioConIVA.ToString();
            ActualizarIVA();
        }

        /// <summary>
        /// Actualiza las propiedades del artículo.
        /// </summary>
        private void Guardar()
        {
            if (PrecioConIVA == 0 || string.IsNullOrWhiteSpace(TextBox_Nombre.Text))
            {
                MessageBox.Show("¡Los datos no se ingresaron correctamente!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                _articulo.Nombre = TextBox_Nombre.Text;
                _articulo.PrecioConIVA = PrecioConIVA;
                bool _correcto;
                if (ModoEdicion)
                {
                    _correcto = Programa.AdMon.EditarArticulo(_articulo.ID, _articulo);
                }
                else
                {
                    _correcto = Programa.AdMon.AgregarNuevoElemento(_articulo.ToString());
                }

                if (_correcto)
                {
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    MessageBox.Show("¡No se pudo guardar los cambios!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        /// <summary>
        /// Actualiza el IVA del producto.
        /// </summary>
        private void ActualizarIVA()
        {
            PrecioSinIVA = Programa.AdMon.ObtenerValorSinIVA(PrecioConIVA);
            TextBox_IVA.Text = (PrecioConIVA - PrecioSinIVA).ToString(Programa.AdMon.FormatoMoneda);
        }

        #endregion

        #region Propiedades

        /// <summary>
        /// Indica si el programa está en modo edición.
        /// </summary>
        private bool ModoEdicion { get; set; } = false;

        /// <summary>
        /// Almacena el precio del artículo sin IVA.
        /// </summary>
        private float PrecioSinIVA { get; set; } = 0F;

        /// <summary>
        /// Almacena el precio del artículo con IVA.
        /// </summary>
        private float PrecioConIVA { get; set; } = 0F;
        #endregion

        #region Eventos

        /// <summary>
        /// Evita que se ingrese un valor no numérico.
        /// </summary>
        private void TextBox_PrecioConIVA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Se produce cuando el texto del campo "PrecioConIVA" cambia.
        /// </summary>
        private void TextBox_PrecioConIVA_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TextBox_PrecioConIVA.Text))
            {
                try
                {
                    PrecioConIVA = Convert.ToSingle(TextBox_PrecioConIVA.Text);
                    ActualizarIVA();
                }
                catch { }
            }
        }

        /// <summary>
        /// Se produce cuando se presiona el botón "Aceptar".
        /// </summary>
        private void Boton_Aceptar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        /// <summary>
        /// Evita que se ingresen caracteres especiales.
        /// </summary>
        private void TextBox_Nombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ';' || e.KeyChar == '=')
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Tiene lugar cuando el formulario adquire el enfoque.
        /// </summary>
        private void Form_EdicionArticulo_Activated(object sender, EventArgs e)
        {
            TextBox_Nombre.Focus();
        }

        #endregion
    }
}
